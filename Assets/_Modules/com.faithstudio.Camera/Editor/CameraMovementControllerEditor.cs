﻿
namespace com.faithstudio.Camera
{
    using UnityEngine;
    using UnityEditor;

    [CustomEditor(typeof(CameraMovementController))]
    public class CameraMovementControllerEditor : Editor
    {
        private CameraMovementController Reference;

        private void OnEnable()
        {
            Reference = (CameraMovementController)target;
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            if (EditorApplication.isPlaying)
            {

                EditorGUILayout.BeginHorizontal();
                {
                    if (GUILayout.Button("Focus Target"))
                    {

                        Reference.FocusCameraWithOrigin(
                                Reference.cameraContainerTransformReference,
                                Reference.editorCameraTarget
                            );
                    }

                    if (GUILayout.Button("Focus Area"))
                    {

                        Reference.FocusCamera(
                                Reference.editorCameraTarget
                            );
                    }
                }
                EditorGUILayout.EndHorizontal();


            }

            EditorGUILayout.Space();
            DrawDefaultInspector();

            serializedObject.ApplyModifiedProperties();
        }
    }
}


