﻿namespace com.faithstudio.Camera {

    using UnityEngine;
    using com.alphapotato.Gameplay;

    public class CameraShake : MonoBehaviour
    {
        #region Public Variables

        public Transform cameraContainerTransformReference;

        [Space(5.0f)]
        [Range(0f, 100f)]
        public float cameraShakeSpeed = 1f;
        [Range(0f, 10f)]
        public float defaultDurationOfCameraShake = 2.5f;

        [Space(5.0f)]
        [Header("Shaking Points")]
        [Range(0f, 10f)]
        public float cameraShakeOnXAxis = 1f;
        [Range(0f, 10f)]
        public float cameraShakeOnYAxis = 1f;
        [Range(0f, 10f)]
        public float cameraShakeOnZAxis = 1f;

        #endregion

        #region Private Variables

        private TimeController m_TimeManagerReference;

        private Vector3 m_TargetedPosition = Vector3.zero;
        private Vector3 m_ModifiedPosition;

        private float m_CurrentDistance = 0f;

        private Vector3 m_VarientInitialPositionOfCameraContainer;
        private Vector3 m_InitialPositionOfCameraContainer;

        private bool m_IsCameraShakeControllerRunning;
        private bool m_IsRepetativeShakeEnabled;

        private float m_AbsoluteCameraShakeDuration;
        private float m_RemainingTimeForCameraShake;
        private float m_AbsluteCameraShakingSpeed;

        #endregion

        #region MonoBehaviour

        private void Awake()
        {
            Initialization();
        }

        private void Start()
        {
            m_TimeManagerReference = TimeController.Instance;
        }

        private void Update() {

            float t_DeltaTime = m_TimeManagerReference.GetAbsoluteDeltaTime();

            if (m_RemainingTimeForCameraShake > 0)
            {
                m_ModifiedPosition = Vector3.Lerp(
                        m_VarientInitialPositionOfCameraContainer,
                        m_VarientInitialPositionOfCameraContainer + new Vector3(
                            Random.Range(-cameraShakeOnXAxis, cameraShakeOnXAxis),
                            Random.Range(-cameraShakeOnYAxis, cameraShakeOnYAxis),
                            Random.Range(-cameraShakeOnZAxis, cameraShakeOnZAxis)
                        ),
                        m_AbsluteCameraShakingSpeed * t_DeltaTime
                    );
                cameraContainerTransformReference.localPosition = m_ModifiedPosition;

                m_CurrentDistance = Vector3.Distance(m_ModifiedPosition, m_TargetedPosition);

                if (!m_IsRepetativeShakeEnabled)
                {
                    m_RemainingTimeForCameraShake -= t_DeltaTime;
                }
            }
            else if (Vector3.Distance(cameraContainerTransformReference.localPosition, m_InitialPositionOfCameraContainer) >= 0.1f) {

                m_ModifiedPosition = Vector3.Lerp(
                        cameraContainerTransformReference.localPosition,
                        m_InitialPositionOfCameraContainer,
                        m_AbsluteCameraShakingSpeed * t_DeltaTime
                    );
                cameraContainerTransformReference.localPosition = m_ModifiedPosition;
            }
            else
            {
                m_IsCameraShakeControllerRunning = false;
                enabled = false;
            }
        }

        #endregion

        #region Configuretion

        private void Initialization()
        {

            enabled = false;

            if (cameraContainerTransformReference == null)
                cameraContainerTransformReference = transform;

            m_InitialPositionOfCameraContainer = cameraContainerTransformReference.localPosition;
            

        }

       

        #endregion

        #region Public Callback

        public void ShowCameraShake()
        {

            ShowCameraShake(defaultDurationOfCameraShake, cameraShakeSpeed, true);
        }

        public void ShowCameraShake(float t_Duration, bool t_RepetativeShake)
        {

            ShowCameraShake(t_Duration, cameraShakeSpeed, t_RepetativeShake);
        }

        public void ShowCameraShake(float t_Duration, float t_CameraShakeSpeed, bool t_RepetativeShake)
        {

            if (!m_IsCameraShakeControllerRunning)
            {
                m_AbsluteCameraShakingSpeed = t_CameraShakeSpeed;
                m_AbsoluteCameraShakeDuration = t_Duration;
                m_RemainingTimeForCameraShake = t_Duration;
                m_IsRepetativeShakeEnabled = t_RepetativeShake;

                m_VarientInitialPositionOfCameraContainer = m_InitialPositionOfCameraContainer;

                m_IsCameraShakeControllerRunning = true;
                enabled = true;
            }
            else
            {

                Debug.LogWarning("CameraShake Controller already running");
            }
        }

        public void StopCameraShake()
        {

            m_RemainingTimeForCameraShake = 0f;
        }

        public void ChangeShakingValue(float t_ShakeSpeed)
        {
            m_AbsluteCameraShakingSpeed = t_ShakeSpeed;
        }

        #endregion
    }
}
