﻿namespace com.faithstudio.Gameplay
{
    using System.Collections.Generic;
    using UnityEngine;

    [CreateAssetMenu(fileName = "LevelInfo", menuName = "Info Container/Create LevelInfo")]
    public class LevelInfoContainer : ScriptableObject
    {
        #region Custom Variables

        [System.Serializable]
        public struct LevelInfo
        {
            public int          numberOfCrowd;
            public int   indexOflevelEnvironmentReference;
        }

        #endregion

        #region Public Variables

        public List<LevelInfo> levelInfos;

        #endregion

        #region Public Callback

#if UNITY_EDITOR

        public void ResetInfos()
        {
            levelInfos = new List<LevelInfo>();
        }

        public void GenerateLevelInfo(
                int t_NumberOfCrowd,
                int t_LevelEnvironmentReference
            )
        {
            levelInfos.Add(new LevelInfo()
            {
                numberOfCrowd = t_NumberOfCrowd,
                indexOflevelEnvironmentReference = t_LevelEnvironmentReference
            });

        }

#endif

        public bool IsValidLevel(int t_Level)
        {

            if (t_Level >= 0 && t_Level < levelInfos.Count)
            {
                return true;
            }
            else
            {
                Debug.LogError("Invalid Level Index : " + t_Level);
                return false;
            }
        }


        public int GetNumberOfCrowd(int t_Level)
        {

            if (IsValidLevel(t_Level))
            {
                return levelInfos[t_Level].numberOfCrowd;
            }
            else
            {

                return -1;
            }
        }

        public int GetEnvironment(int t_Level){
            
            if (IsValidLevel(t_Level))
            {
                return levelInfos[t_Level].indexOflevelEnvironmentReference;
            }
            else
            {

                return -1;
            }
        }

        #endregion
    }

}

