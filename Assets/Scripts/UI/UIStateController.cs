﻿using UnityEngine;
using UnityEngine.Events;

public class UIStateController : MonoBehaviour
{
    #region Public Variables

    public static UIStateController Instance;

    [Header("External Reference")]
    public UIMessageDialugeController UIMessageDialougeController;

    [Space(5.0f)]
    public UIMainMenuController     UIMainMenuControllerReference;
    public UIGameplayMenuController UIGameplayControllerReference;
    public UIGameOverMenuController UIGameOverMenuControllerReference;

    #endregion

    #region Private Variables

    #endregion

    #region Mono Behaviour

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        
    }

    #endregion

    #region Configuretion

    #endregion

    #region Public Callback :   MainMenu

    public void AppearMainMenu(UnityAction OnAppearTransationComplete = null, float t_DelayBeforeInvokingOnTransationComplete = 0f) {

        UIMainMenuControllerReference.AppearMainMenu(OnAppearTransationComplete, t_DelayBeforeInvokingOnTransationComplete);
    }

    public void DisappearMainMenu(UnityAction OnAppearTransationComplete = null, float t_DelayBeforeInvokingOnTransationComplete = 0f) {

        UIMainMenuControllerReference.DisappearMainMenu(OnAppearTransationComplete, t_DelayBeforeInvokingOnTransationComplete);
    }

    #endregion

    #region Public Callback :   GameplayMenu

    public void AppearGameplayMenu(
        Sprite ppForPlayer,
        Sprite ppForOpponent,
        UnityAction OnAppearTransationComplete = null,
        float t_DelayBeforeInvokingOnTransationComplete = 0f)
    {
        UIGameplayControllerReference.AppearGameplayMenu(
            ppForPlayer,
            ppForOpponent,
            OnAppearTransationComplete,
            t_DelayBeforeInvokingOnTransationComplete);
    }

    public void DisappearGameplayMenu(UnityAction OnAppearTransationComplete = null, float t_DelayBeforeInvokingOnTransationComplete = 0f)
    {
        UIGameplayControllerReference.DisappearGameplayMenu(OnAppearTransationComplete, t_DelayBeforeInvokingOnTransationComplete);
    }

    public void AppearSpeedBoosterButton()
    {
        UIGameplayControllerReference.AppearSpeedBoosterButton();
    }

    public void DisappearSpeedBoosterButton()
    {
        UIGameplayControllerReference.DisappearSpeedBoosterButton();
    }

    public void AppearProgressBarForSpeedBooster() {

        UIGameplayControllerReference.AppearProgressBarForSpeedBooster();
    }

    public void DisappearProgressBarForSpeedBooster() {

        UIGameplayControllerReference.DisappearProgressBarForSpeedBooster();
    }

    public void AppearTimerForAimingBar(float t_Duration){

        UIGameplayControllerReference.AppearTimerForAimingBar(t_Duration);
    }

    public void DisappearTimerForAimingBar(){

        UIGameplayControllerReference.DisappearTimerForAimingBar();
    }

    public void UpdatePlayerHealth(float t_CurrentHealthOfPlayer, float t_MaxHealthOfPlayer, float t_DamageOnThisRound = -1f) {

        UIGameplayControllerReference.UpdatePlayerHealth(t_CurrentHealthOfPlayer, t_MaxHealthOfPlayer, t_DamageOnThisRound);
    }

    public void UpdateOpponentHealth(float t_CurrentHealthOfOpponent, float t_MaxHealthOfOpponenet, float t_DamageOnThisRound = -1f) {

        UIGameplayControllerReference.UpdateOpponentHealth(t_CurrentHealthOfOpponent, t_MaxHealthOfOpponenet, t_DamageOnThisRound);
    }

    public void UpdateRoundNumber(int t_RoundNumber){

        UIGameplayControllerReference.UpdateRoundNumber(t_RoundNumber);
    }

    public void UpdateCoinEarnForThisLevel(double t_Amount){

        UIGameplayControllerReference.UpdateCoinEarnForThisLevel(t_Amount);
    }

    public void ShowTransationForRoundEnd(int t_RoundIndex){

        UIGameplayControllerReference.ShowTransationForRoundEnd(t_RoundIndex);
    }

    #endregion

    #region  Public Callback    :   GameOverMenu

    public void AppearGameOverMenu(bool t_IsMatchWon, double t_AmountOfCoinClaimed){

        UIGameOverMenuControllerReference.AppearGameOverMenu(t_IsMatchWon,t_AmountOfCoinClaimed);
    }

    public void DisappearGameOverMenu(){

        UIGameOverMenuControllerReference.DisappearGameOverMenu();
    }

    #endregion
}
