﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

using com.faithstudio.UI;
using com.faithstudio.Math;

public class UIGameOverMenuController : MonoBehaviour
{
    #region Public Variables

    [Header("Reference      :   External")]
    public UISceneLoadController UISceneLoadController;

    [Header("Configuretion  :   GameOverPanel")]
    public Animator animatorReferenceForGameOverMenu;
    public Animator animatorReferenceForCelebretionCrown;

    [Space(5.0f)]
    public TextMeshProUGUI textReferenceForMatchResult;
    public TextMeshProUGUI textReferenceForTotalCoinEarned;

    [Space(5.0f)]
    public Button buttonForClaim;

    #endregion

    #region  Private Variables

    private bool m_IsGameOverMenuShowing;
    private bool m_IsMatchWon;
    #endregion

    #region Mono Behaviour

    private void Awake() {
        
        buttonForClaim.onClick.AddListener(delegate{
            
            DisappearGameOverMenu();
            UISceneLoadController.LoadSceneWithProgressBar("GameScene");
        });
    }

    #endregion

    #region Configuretion

    #endregion

    #region Public Callback

    public void AppearGameOverMenu(bool t_IsMatchWon, double t_AmountOfCoinClaimed){

        

        if(!m_IsGameOverMenuShowing){

            m_IsMatchWon            = t_IsMatchWon;
            m_IsGameOverMenuShowing = true;
            
            textReferenceForTotalCoinEarned.text = MathFunction.Instance.GetCurrencyInFormatInNonDecimal(t_AmountOfCoinClaimed);
            if(m_IsMatchWon){
                textReferenceForMatchResult.text = "YOU WIN!!";
                animatorReferenceForCelebretionCrown.SetTrigger("APPEAR");
            }else{
                textReferenceForMatchResult.text = "YOU LOSE!";
            }

            animatorReferenceForGameOverMenu.SetTrigger("APPEAR");
        }
    }

    public void DisappearGameOverMenu(){

        if(m_IsGameOverMenuShowing){

            if(m_IsMatchWon)
                animatorReferenceForCelebretionCrown.SetTrigger("DISAPPEAR");

            animatorReferenceForGameOverMenu.SetTrigger("DISAPPEAR");
            m_IsGameOverMenuShowing = false;
        }
    }

    #endregion
}
