﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;

using System.Collections;

using com.faithstudio.Math;
using com.faithstudio.Gameplay;

public class UIMainMenuController : MonoBehaviour
{
    #region Public Variables

    public Animator animatorReference;

    [Header("Reference  :   External")]
    public PlayerController playerControllerReference;
    public LevelManager     levelManagerReference;

    [Header("Parameter  :   StartButton & Info")]
    public TextMeshProUGUI textReferenceOfLevelInfo;
    public Button startButton;

    [Header("Parameter  :   CoinEarnPanel")]
    public TextMeshProUGUI  textReferenceForTotalCoinEarn;
    public Animator         animatorReferenceForCoinEarnPanel;

    

    [Header("Parameter  : HealthUpgrade")]
    public TextMeshProUGUI textReferenceForHealthState;
    public TextMeshProUGUI textReferenceForHealthUpgradeCost;
    public Animator animatorReferenceForHealthUpgradeButton;
    public Button buttonReferenceForHealthUpgrade;

    [Header("Parameter  : PowerUpgrade")]
    public TextMeshProUGUI textReferenceForAttackPowerState;
    public TextMeshProUGUI textReferenceForAttackPowerUpgradeCost;
    public Animator animatorReferenceForAttackPowerUpgradeButton;
    public Button buttonReferenceForAttackPowerUpgrade;

    [Header("Parameter  : CoinEarnUpgrade")]
    public TextMeshProUGUI textReferenceForCoinEarnMultiplierState;
    public TextMeshProUGUI textReferenceForCoinEarnMultiplierUpgradeCost;
    public Animator animatorReferenceForCoinEarnMultiplierUpgradeButton;
    public Button buttonReferenceForCoinEarnMultiplierUpgrade;

    #endregion

    #region Private Variables

    private bool m_IsMainMeniShowing;

    #endregion

    #region Mono Behaviour

    private void Awake()
    {
        startButton.onClick.AddListener(delegate
        {
            GameplayController.Instance.ActivateEnvironmentBasedOnLevel();
            DisappearMainMenu(delegate
            {
                UIStateController.Instance.UpdateCoinEarnForThisLevel(0);
                UIStateController.Instance.AppearGameplayMenu(
                    GameplayController.Instance.playerControllerReference.joustingKnightControllerReference.knightFactory.GetSelectedCharacterPP(),
                    GameplayController.Instance.enemyControllerReference.joustingKnightControllerReference.knightFactory.GetSelectedCharacterPP(),
                    delegate
                    {
                        GameplayController.Instance.PreProcess();
                    },
                    0.5f);
            }, 0.25f);
        });

        buttonReferenceForHealthUpgrade.onClick.AddListener(delegate
        {

            if (m_IsMainMeniShowing)
            {

                double t_UpgradeCost = playerControllerReference.GetUpgradeCostForHealth();
                if (GameManager.Instance.GetInGameCurrency() >= t_UpgradeCost)
                {

                    animatorReferenceForHealthUpgradeButton.SetTrigger("TAP");
                    
                    GameManager.Instance.DeductInGameCurrency(t_UpgradeCost);
                    textReferenceForTotalCoinEarn.text = MathFunction.Instance.GetCurrencyInFormat(GameManager.Instance.GetInGameCurrency());

                    playerControllerReference.UpgradeHealth();
                    UpdateUIForHealthUpgradePanel();

                }
            }

        });

        buttonReferenceForAttackPowerUpgrade.onClick.AddListener(delegate
        {

            if (m_IsMainMeniShowing)
            {

                double t_UpgradeCost = playerControllerReference.GetUpgradeCostForAttackPower();
                if (GameManager.Instance.GetInGameCurrency() >= t_UpgradeCost)
                {

                    animatorReferenceForAttackPowerUpgradeButton.SetTrigger("TAP");
                    
                    GameManager.Instance.DeductInGameCurrency(t_UpgradeCost);
                    textReferenceForTotalCoinEarn.text = MathFunction.Instance.GetCurrencyInFormat(GameManager.Instance.GetInGameCurrency());

                    playerControllerReference.UpgradeAttackPower();
                    UpdateUIForAttackPowerPanel();
                    
                }
            }

        });

        buttonReferenceForCoinEarnMultiplierUpgrade.onClick.AddListener(delegate
        {

            if (m_IsMainMeniShowing)
            {

                double t_UpgradeCost = playerControllerReference.GetUpgradeCostForCoinEarnMultiplier();
                if (GameManager.Instance.GetInGameCurrency() >= t_UpgradeCost)
                {

                    animatorReferenceForCoinEarnMultiplierUpgradeButton.SetTrigger("TAP");
                    
                    GameManager.Instance.DeductInGameCurrency(t_UpgradeCost);
                    textReferenceForTotalCoinEarn.text = MathFunction.Instance.GetCurrencyInFormat(GameManager.Instance.GetInGameCurrency());

                    playerControllerReference.UpgradeCoinEarnMultiplier();
                    UpdateUIForCoinEarnMultiplierPanel();

                }
            }

        });

        m_IsMainMeniShowing = false;
        
    }

    private void Start(){

        UpdateUIForHealthUpgradePanel();
        UpdateUIForAttackPowerPanel();
        UpdateUIForCoinEarnMultiplierPanel();

        AppearMainMenu();
    }

    #endregion

    #region Configuretion

    private void UpdateUIForHealthUpgradePanel(){

        textReferenceForHealthState.text        = playerControllerReference.GetPlayerHealth().ToString("F0");
        textReferenceForHealthUpgradeCost.text  = MathFunction.Instance.GetCurrencyInFormatInNonDecimal(playerControllerReference.GetUpgradeCostForHealth());
    }

    private void UpdateUIForAttackPowerPanel(){

        textReferenceForAttackPowerState.text        = playerControllerReference.GetPlayerAttackPower().ToString("F0");
        textReferenceForAttackPowerUpgradeCost.text  = MathFunction.Instance.GetCurrencyInFormatInNonDecimal(playerControllerReference.GetUpgradeCostForAttackPower());
    }

    private void UpdateUIForCoinEarnMultiplierPanel(){

        textReferenceForCoinEarnMultiplierState.text        = playerControllerReference.GetCoinEarnMultiplier().ToString("F2");
        textReferenceForCoinEarnMultiplierUpgradeCost.text  = MathFunction.Instance.GetCurrencyInFormatInNonDecimal(playerControllerReference.GetUpgradeCostForCoinEarnMultiplier());
    }

    private IEnumerator ControllerForAppear(UnityAction OnAppearTransationComplete = null, float t_DelayBeforeInvokingOnTransationComplete = 0f)
    {

        WaitForSeconds t_DelayForUpgradeButtonToAppear = new WaitForSeconds(0.1f);

        textReferenceOfLevelInfo.text = "LEVEL " + (levelManagerReference.GetCurrentLevel() + 1);
        textReferenceForTotalCoinEarn.text = MathFunction.Instance.GetCurrencyInFormat(GameManager.Instance.GetInGameCurrency());
        animatorReferenceForCoinEarnPanel.SetTrigger("APPEAR");

        animatorReference.SetTrigger("APPEAR");
        yield return t_DelayForUpgradeButtonToAppear;
        animatorReferenceForHealthUpgradeButton.SetTrigger("APPEAR");
        yield return t_DelayForUpgradeButtonToAppear;
        animatorReferenceForAttackPowerUpgradeButton.SetTrigger("APPEAR");
        yield return t_DelayForUpgradeButtonToAppear;
        animatorReferenceForCoinEarnMultiplierUpgradeButton.SetTrigger("APPEAR");

        yield return new WaitForSeconds(t_DelayBeforeInvokingOnTransationComplete == 0 ? 1f : t_DelayBeforeInvokingOnTransationComplete);

        OnAppearTransationComplete?.Invoke();

        StopCoroutine(ControllerForAppear());
    }

    private IEnumerator ControllerForDisappear(UnityAction OnAppearTransationComplete = null, float t_DelayBeforeInvokingOnTransationComplete = 0f)
    {
        WaitForSeconds t_DelayForUpgradeButtonToDisappear = new WaitForSeconds(0.1f);

        animatorReferenceForCoinEarnPanel.SetTrigger("DISAPPEAR");
        animatorReference.SetTrigger("DISAPPEAR");
        yield return t_DelayForUpgradeButtonToDisappear;
        animatorReferenceForHealthUpgradeButton.SetTrigger("DISAPPEAR");
        yield return t_DelayForUpgradeButtonToDisappear;
        animatorReferenceForAttackPowerUpgradeButton.SetTrigger("DISAPPEAR");
        yield return t_DelayForUpgradeButtonToDisappear;
        animatorReferenceForCoinEarnMultiplierUpgradeButton.SetTrigger("DISAPPEAR");

        yield return new WaitForSeconds(t_DelayBeforeInvokingOnTransationComplete == 0 ? 1f : t_DelayBeforeInvokingOnTransationComplete);

        OnAppearTransationComplete?.Invoke();

        StopCoroutine(ControllerForDisappear());
    }

    #endregion

    #region Public Callback

    public void AppearMainMenu(UnityAction OnAppearTransationComplete = null, float t_DelayBeforeInvokingOnTransationComplete = 0f)
    {
        if (!m_IsMainMeniShowing)
        {

            m_IsMainMeniShowing = true;
            StartCoroutine(ControllerForAppear(OnAppearTransationComplete, t_DelayBeforeInvokingOnTransationComplete));
        }

    }

    public void DisappearMainMenu(UnityAction OnAppearTransationComplete = null, float t_DelayBeforeInvokingOnTransationComplete = 0f)
    {
        if (m_IsMainMeniShowing)
        {
            m_IsMainMeniShowing = false;
            StartCoroutine(ControllerForDisappear(OnAppearTransationComplete, t_DelayBeforeInvokingOnTransationComplete));
        }
    }

    #endregion
}
