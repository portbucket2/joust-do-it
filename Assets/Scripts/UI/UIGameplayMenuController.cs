﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;

using System.Collections;

using com.faithstudio.Math;
using com.faithstudio.Gameplay;

public class UIGameplayMenuController : MonoBehaviour
{
    #region Public Variables

    

    [Header("Configuretion  :   CoinPanel")]
    public TextMeshProUGUI  textRefrenceOfTotalCoinEarnInThisLevel;
    public Animator         animatorReferenceForCoinEarnPanel;

    [Header("Configuretion  :   GameProgressionPanel")]
    public TextMeshProUGUI  textReferenceForRoundNumber;
    public Animator         animatorReferenceForGameProgressionPanel;
    [Space(5.0f)]
    public TextMeshProUGUI  healthTextForPlayer;
    public Image            healthBarForPlayer;
    public Image            imageForPlayerPP;
    [Space(5.0f)]
    public TextMeshProUGUI  healthTextForOpponent;
    public Image            healthBarForOpponent;
    public Image            imageForOpponentPP;
    [Space(5.0f)]
    public Gradient         healthBarColorOnDifferentState;

    [Header("Configuretion  :   SpeedBoostButton")]
    public Animator animatorForSpeedBooster;
    public Button   buttonForSpeedBooster;

    [Header("Configuretion  :   SpeedBoostProgressBar")]
    public Animator animatorForProgressBarOfSpeedBooster;
    public Image    progressBarForSpeedBoost;
    public Gradient progressBarColorThroughProgression;

    [Header("Configuretion  :   TimerForAiming")]
    public RectTransform    timerIndicationBar;

    [Header("Configuretion  :   RoundEndTransation")]
    public Animator         animatorReferenceForRoundEndTransation;
    public TextMeshProUGUI  textReferenceForEndedRoundNumber;

    #endregion

    #region Private Variables

    private bool    m_IsAppearingMainMenu;
    private bool    m_IsDisappearingMainMenu;
    private bool    m_IsProgressBarVisible;
    private bool    m_IsTimerForAimingVisible;

    private float   m_CurrentValueForProgressBarForSpeedBooster;

    #endregion

    #region Mono Behaviour

    private void Awake()
    {

        if (progressBarForSpeedBoost.type != Image.Type.Filled)
            progressBarForSpeedBoost.type = Image.Type.Filled;

        
        buttonForSpeedBooster.onClick.AddListener(delegate
        {
            if (buttonForSpeedBooster.IsInteractable()) {

                animatorForSpeedBooster.SetTrigger("TAP");
                GameplayController.Instance.OnButtonPressedForSpeedIncrease();
            }
            
        });
        buttonForSpeedBooster.interactable = false;
    }

    #endregion

    #region Configuretion

    private IEnumerator ControllerForAppear(UnityAction OnAppearTransationComplete = null, float t_DelayBeforeInvokingOnTransationComplete = 0f) {

        m_CurrentValueForProgressBarForSpeedBooster     = 0;

        animatorReferenceForGameProgressionPanel.SetTrigger("APPEAR");
        animatorReferenceForCoinEarnPanel.SetTrigger("APPEAR");

        yield return new WaitForSeconds(t_DelayBeforeInvokingOnTransationComplete == 0 ? 1f : t_DelayBeforeInvokingOnTransationComplete);

        OnAppearTransationComplete?.Invoke();

        StopCoroutine(ControllerForAppear());
        m_IsAppearingMainMenu = false;
    }

    private IEnumerator ControllerForDisappear(UnityAction OnAppearTransationComplete = null, float t_DelayBeforeInvokingOnTransationComplete = 0f) {

        animatorReferenceForGameProgressionPanel.SetTrigger("DISAPPEAR");
        animatorReferenceForCoinEarnPanel.SetTrigger("DISAPPEAR");
        DisappearSpeedBoosterButton();
        DisappearProgressBarForSpeedBooster();

        yield return new WaitForSeconds(t_DelayBeforeInvokingOnTransationComplete == 0 ? 1f : t_DelayBeforeInvokingOnTransationComplete);

        OnAppearTransationComplete?.Invoke();

        StopCoroutine(ControllerForDisappear());
        m_IsDisappearingMainMenu = false;
    }

    private IEnumerator ControllerForFillingProgressBar() {

        float           t_CycleLength   = 0.0167f;
        WaitForSeconds  t_CycleDelay    = new WaitForSeconds(t_CycleLength);

        GameplayController  t_GameplayControllerReference               = GameplayController.Instance;
        float t_Progression;

        while (m_IsProgressBarVisible) {

            t_Progression = t_GameplayControllerReference.GetCurrentInterpolatedVelocity();

            m_CurrentValueForProgressBarForSpeedBooster = Mathf.Lerp(
                    m_CurrentValueForProgressBarForSpeedBooster,
                    t_Progression,
                    0.1f * (1f - (t_Progression * 0.5f))
                );

            progressBarForSpeedBoost.fillAmount = m_CurrentValueForProgressBarForSpeedBooster;
            progressBarForSpeedBoost.color      = progressBarColorThroughProgression.Evaluate(m_CurrentValueForProgressBarForSpeedBooster);

            if(m_CurrentValueForProgressBarForSpeedBooster >= 0.95f){

                TutorialController.Instance.HideTutorial();
                DisappearProgressBarForSpeedBooster();
                DisappearSpeedBoosterButton();
            }

            yield return t_CycleDelay;
        }
    }

    private IEnumerator ControllerForTimerForAiming(float t_Duration){

        float t_CycleLength = 0.033f;
        WaitForSecondsRealtime t_CycleDelay = new WaitForSecondsRealtime(t_CycleLength);

        float t_RemainingTime = t_Duration;
        float t_Progression;
        Vector3 t_ModifiedLocalScale = Vector3.one;
        while(m_IsTimerForAimingVisible && t_RemainingTime > 0){

            t_Progression = t_RemainingTime / t_Duration;
            t_ModifiedLocalScale.x = t_Progression;
            t_ModifiedLocalScale.y = Mathf.Abs(Mathf.Sin(t_Progression * 32)) + 0.25f;
            timerIndicationBar.localScale = t_ModifiedLocalScale;
            yield return t_CycleDelay;
            t_RemainingTime -= t_CycleLength;
        }

        timerIndicationBar.localScale = Vector3.zero;
        m_IsTimerForAimingVisible = false;
        StopCoroutine(ControllerForTimerForAiming(0));
    }

    private IEnumerator ControllerForCoinEarning(int t_NumberOfCoinToBeAdded){

        yield return new WaitForSecondsRealtime(0.5f);

        float t_CycleLength = 0.5f;
        WaitForSecondsRealtime t_CycleDelay = new WaitForSecondsRealtime(t_CycleLength);
        
        while(t_NumberOfCoinToBeAdded > 0){

            CoinEarningController.Instance.AddCoinToUser(
                (Vector3.up * 3) + GameplayController.Instance.enemyControllerReference.joustingKnightControllerReference.knightTransformReference.position,
                Vector3.zero,
                Vector3.zero,
                GameplayController.Instance.playerControllerReference.GetCoinEarnMultiplier(),
                true
            );
            t_NumberOfCoinToBeAdded--;
            yield return t_CycleLength;
        }

        StopCoroutine(ControllerForCoinEarning(0));
    }

    #endregion

    #region Public Callback

    public void AppearGameplayMenu(
        Sprite ppForPlayer,
        Sprite ppForOpponent,
        UnityAction OnAppearTransationComplete = null,
        float t_DelayBeforeInvokingOnTransationComplete = 0f) {
        
        if (!m_IsAppearingMainMenu)
        {
            m_IsAppearingMainMenu = true;

            imageForPlayerPP.sprite = ppForPlayer;
            imageForOpponentPP.sprite = ppForOpponent;

            StartCoroutine(ControllerForAppear(OnAppearTransationComplete, t_DelayBeforeInvokingOnTransationComplete));
        }
    }


    public void DisappearGameplayMenu(
        UnityAction OnAppearTransationComplete = null,
        float t_DelayBeforeInvokingOnTransationComplete = 0f) {

        if (!m_IsDisappearingMainMenu)
        {
            m_IsDisappearingMainMenu = true;
            StartCoroutine(ControllerForDisappear(OnAppearTransationComplete, t_DelayBeforeInvokingOnTransationComplete));
        }
        
    }

    public void AppearSpeedBoosterButton() {

        if(!buttonForSpeedBooster.interactable){

            buttonForSpeedBooster.interactable = true;
            animatorForSpeedBooster.SetTrigger("APPEAR");
        }
    }

    public void DisappearSpeedBoosterButton() {

        if(buttonForSpeedBooster.interactable){

            buttonForSpeedBooster.interactable = false;
            animatorForSpeedBooster.SetTrigger("DISAPPEAR");
        }
    }

    public void AppearProgressBarForSpeedBooster() {

        if (!m_IsProgressBarVisible) {

            animatorForProgressBarOfSpeedBooster.SetTrigger("APPEAR");

            m_IsProgressBarVisible = true;
            StartCoroutine(ControllerForFillingProgressBar());
        }
    }

    public void DisappearProgressBarForSpeedBooster() {

        if(m_IsProgressBarVisible){

            m_IsProgressBarVisible = false;
            animatorForProgressBarOfSpeedBooster.SetTrigger("DISAPPEAR");
        }
    }

    public void AppearTimerForAimingBar(float t_Duration){

        if(!m_IsTimerForAimingVisible){

            m_IsTimerForAimingVisible = true;
            StartCoroutine(ControllerForTimerForAiming(t_Duration));
        }
    }

    public void DisappearTimerForAimingBar(){

        m_IsTimerForAimingVisible = false;
    }

    public void UpdateRoundNumber(int t_RoundNumber){

        textReferenceForRoundNumber.text = "ROUND " + t_RoundNumber;
    }

    public void UpdatePlayerHealth(float t_CurrentHealthOfPlayer, float t_MaxHealthOfPlayer, float t_DamageOnThisRound = -1) {
        
        //healthTextForPlayer.text = t_CurrentHealthOfPlayer.ToString("F0") + "/" + t_MaxHealthOfPlayer.ToString("F0");
        healthTextForPlayer.text = t_CurrentHealthOfPlayer.ToString("F0");

        float t_LifeState = t_CurrentHealthOfPlayer / t_MaxHealthOfPlayer;
        healthBarForPlayer.fillAmount = t_LifeState;
        healthBarForPlayer.color = healthBarColorOnDifferentState.Evaluate(t_LifeState);
    }

    public void UpdateOpponentHealth(float t_CurrentHealthOfOpponent, float t_MaxHealthOfOpponenet, float t_DamageOnThisRound = -1) {

        //healthTextForOpponent.text = t_CurrentHealthOfOpponent.ToString("F0") + "/" + t_MaxHealthOfOpponenet.ToString("F0");
        healthTextForOpponent.text = t_CurrentHealthOfOpponent.ToString("F0");

        float t_LifeState = t_CurrentHealthOfOpponent / t_MaxHealthOfOpponenet;
        healthBarForOpponent.fillAmount = t_LifeState;
        healthBarForOpponent.color = healthBarColorOnDifferentState.Evaluate(t_LifeState);

        int t_NumberOfCoinToBeAdded = Mathf.CeilToInt(t_DamageOnThisRound / 10);
        StartCoroutine(ControllerForCoinEarning(t_NumberOfCoinToBeAdded));
    }

    public void UpdateCoinEarnForThisLevel(double t_Amount){

        textRefrenceOfTotalCoinEarnInThisLevel.text = MathFunction.Instance.GetCurrencyInFormatInNonDecimal(t_Amount);
    }

    public void ShowTransationForRoundEnd(int t_RoundIndex){
        
        textReferenceForEndedRoundNumber.text = "ROUND " + t_RoundIndex;
        animatorReferenceForRoundEndTransation.SetTrigger("APPEAR");
    }

    #endregion
}
