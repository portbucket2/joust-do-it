﻿using System.Collections;
using UnityEngine;
using TMPro;

public class UIMessageDialugeController : MonoBehaviour
{
    #region Public Variables

    public Animator         animatorReference;
    public TextMeshProUGUI  messageDialougeText;

    [Space(5.0f)]
    public Color    defaultColorForMessageDialouge;
    [Range(0f,10f)]
    public float    defaultDelayForMessageDialouge;

    #endregion

    #region Private Variables

    private bool    m_IsShowMessageControlleRunning;
    private float   m_RemainingTimeForDelay;
    private float   m_DelayForMessageDialouge;

    #endregion

    #region Mono Behaviour

    #endregion

    #region Configuretion

    private IEnumerator ControllerForMessageDialouge()
    {
        float t_CycleLength         = 0.0167f;
        WaitForSeconds t_CycleDelay = new WaitForSeconds(t_CycleLength);

        while (m_IsShowMessageControlleRunning && m_RemainingTimeForDelay > 0) {

            m_RemainingTimeForDelay -= t_CycleLength;
            yield return t_CycleDelay;
        }

        StopCoroutine(ControllerForMessageDialouge());
        animatorReference.SetTrigger("DISAPPEAR");

        m_IsShowMessageControlleRunning = false;
    }

    #endregion

    #region Public Callback

    public void ShowMessage(string t_Message, Color t_ColorForMessageDialouge = new Color(), float t_DelayForMessageDialouge = 0) {

        if (t_ColorForMessageDialouge == new Color())
            t_ColorForMessageDialouge = defaultColorForMessageDialouge;

        if (t_DelayForMessageDialouge == 0)
            m_DelayForMessageDialouge = defaultDelayForMessageDialouge;
        else
            m_DelayForMessageDialouge = t_DelayForMessageDialouge;

        m_RemainingTimeForDelay     = m_DelayForMessageDialouge;

        messageDialougeText.text    = t_Message;
        messageDialougeText.color   = t_ColorForMessageDialouge;

        animatorReference.SetTrigger("APPEAR");

        if (!m_IsShowMessageControlleRunning) {

            m_IsShowMessageControlleRunning = true;
            StartCoroutine(ControllerForMessageDialouge());
        }
    }

    #endregion
}
