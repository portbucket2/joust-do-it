﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

[System.Serializable]
public class CharacterMovementController : MonoBehaviour
{

    #region Public Variables

    public Transform defaultTransformReferenceOfMovingObject;

    [Space(5.0f)]
    [Range(0f, 25f)]
    public float defaultInitialForwardVelocity = 1f;
    [Range(0f, 100f)]
    public float defaultMaxForwardVelocity = 5f;

    [Space(5.0f)]
    [Range(0f, 1f)]
    public float            defaultAcceleration = 0.01f;
    
    public AnimationCurve   rateOfChangeOfAcceleration = new AnimationCurve(new Keyframe[] { new Keyframe(0, 1f), new Keyframe(1, 0.25f)});

    [Space(5.0f)]
    [Range(0f,1f)]
    public float            defaultDeceleratingPoint;
    public AnimationCurve   rateOfChangeOfDeceleration = new AnimationCurve(new Keyframe[] { new Keyframe(0, 0f), new Keyframe(1, 0.75f) });


    #endregion

    #region Private Variables

    private bool        m_IsControllerForMovementRunning;
    private bool        m_IsKeepTrackingTheTarget;

    private float       m_MaxDistance;

    private float       m_InitialForwardVelocity;
    private float       m_MaxForwardVelocity = 1;
    private float       m_Acceleration;
    private float       m_DecelerationPoint;

    private float       m_TargetedVelocity;

    private Transform   m_TransformReferenceOfTarget;
    private Transform   m_TransformReferenceOfMovingObject;

    private float       m_PeakVelocityForDeceleration = 0;
    private float       m_ProgressTowardTarget = 0;
    private float       m_ProgressForDeceleration = 0;

    private Vector3     m_InitialPosition;
    private Vector3     m_CurrentVelocity = Vector3.zero;
    private Vector3     m_CurrentPosition;
    private Vector3     m_TargetPosition;
    private Vector3     m_ModifiedPosition;

    private Quaternion  m_InitialRotation;
    private Quaternion  m_ModifiedRotation;

    private UnityAction OnMovementEnd;

    #endregion

    #region Mono Behaviour

    private void Awake()
    {
        m_InitialPosition = defaultTransformReferenceOfMovingObject.position;
        m_InitialRotation = defaultTransformReferenceOfMovingObject.rotation;
    }

    #endregion

    #region Configuretion

    private void Update() {

        if (m_IsControllerForMovementRunning) {

            m_CurrentPosition = m_TransformReferenceOfMovingObject.position;
            m_TargetPosition = m_TransformReferenceOfTarget.position;

            m_ProgressTowardTarget = Mathf.SmoothStep(
                                        1f,
                                        0f,
                                        Vector3.Distance(m_CurrentPosition, m_TargetPosition) / m_MaxDistance);



            if (m_ProgressTowardTarget <= m_DecelerationPoint)
            {

                if (m_TargetedVelocity < m_MaxForwardVelocity)
                {
                    m_TargetedVelocity += m_Acceleration * rateOfChangeOfAcceleration.Evaluate(m_ProgressTowardTarget);
                    m_PeakVelocityForDeceleration = m_TargetedVelocity;
                    //Debug.Log("Acceleration -> " + t_ProgressTowardTarget + " : " + m_CurrentVelocity + " : " + m_MaxForwardVelocity);
                }
            }
            else
            {
                m_ProgressForDeceleration = ((m_ProgressTowardTarget - m_DecelerationPoint) / (1f - m_DecelerationPoint));
                m_TargetedVelocity = m_PeakVelocityForDeceleration - (m_PeakVelocityForDeceleration * rateOfChangeOfDeceleration.Evaluate(m_ProgressForDeceleration));

                //Debug.Log("Deceleration -> " + t_PeakVelocityForDeceleration + " : " + t_ProgressForDeceleration);
            }

            m_ModifiedPosition = Vector3.SmoothDamp(
                        m_CurrentPosition,
                        m_TargetPosition,
                        ref m_CurrentVelocity,
                        0.5f,
                        m_TargetedVelocity
                    );


            m_TransformReferenceOfMovingObject.position = m_ModifiedPosition;

            m_ModifiedRotation = Quaternion.Slerp(
                    m_TransformReferenceOfMovingObject.rotation,
                    Quaternion.LookRotation(m_TargetPosition - m_ModifiedPosition),
                    0.1f
                );

            m_TransformReferenceOfMovingObject.rotation = m_ModifiedRotation;

            if (!m_IsKeepTrackingTheTarget && m_ProgressTowardTarget >= 0.99f)
            {
                m_IsControllerForMovementRunning = false;
                OnMovementEnd?.Invoke();
                enabled = false;
            }
        }
    }

    #endregion

    #region Public Callback

    public bool IsMovementControllerRunning() {

        return m_IsControllerForMovementRunning;
    }

    public float GetCurrentInterpolatedVelocity() {

        return Mathf.Clamp01(m_TargetedVelocity / m_MaxForwardVelocity);
    }

    public void Move(
            Transform   transformReferenceOfTarget,
            Transform   transformReferenceOfMovingObject = null,
            bool        isKeepTrackingTheTarget = true,
            float       initialForwardVelocity = 0,
            float       maxForwardVelocity = 0,
            float       acceleration = 0,
            float       decelerationPoint = 0f,
            UnityAction OnMovementEnd = null)
    {

        m_TransformReferenceOfTarget        = transformReferenceOfTarget;
        m_IsKeepTrackingTheTarget           = isKeepTrackingTheTarget;

        if (transformReferenceOfMovingObject == null)
            m_TransformReferenceOfMovingObject = defaultTransformReferenceOfMovingObject;
        else
            m_TransformReferenceOfMovingObject  = transformReferenceOfMovingObject;

        if (maxForwardVelocity == 0)
            m_MaxForwardVelocity = defaultMaxForwardVelocity;
        else
            m_MaxForwardVelocity = maxForwardVelocity;

        if (initialForwardVelocity == 0)
            m_InitialForwardVelocity = defaultInitialForwardVelocity;
        else
            m_InitialForwardVelocity = Mathf.SmoothStep(0, m_MaxForwardVelocity, initialForwardVelocity);

        if (acceleration == 0)
            m_Acceleration = defaultAcceleration;
        else
            m_Acceleration = Mathf.SmoothStep(0f, 0.1f, acceleration);


        if (decelerationPoint == 0)
            m_DecelerationPoint = defaultDeceleratingPoint;
        else
            m_DecelerationPoint = Mathf.SmoothStep(0, 1, decelerationPoint);

        this.OnMovementEnd = OnMovementEnd;

        m_MaxDistance = Vector3.Distance(m_TransformReferenceOfMovingObject.position, m_TransformReferenceOfTarget.position);

        m_TargetedVelocity = m_InitialForwardVelocity;

        m_IsControllerForMovementRunning = true;
        enabled = true;
    }

    public void StopMove(bool t_IsResetToInitialPosition = false) {

        m_TargetedVelocity = 0;
        m_MaxForwardVelocity = 1;
        m_IsControllerForMovementRunning = false;
        enabled = false;

        if (t_IsResetToInitialPosition) {

            defaultTransformReferenceOfMovingObject.position = m_InitialPosition;
            defaultTransformReferenceOfMovingObject.rotation = m_InitialRotation;
        }

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="t_Percentage"> The value would be clamp in the range of [0,1]</param>
    public void IncreaseAcceleration(float t_Percentage) {

        t_Percentage = Mathf.Clamp01(t_Percentage) ;
        m_Acceleration += m_Acceleration * t_Percentage;
    }

    #endregion


}
