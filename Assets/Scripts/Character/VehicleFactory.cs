﻿using UnityEngine;

public enum RiderPose
{
        standing,
        sitting
}

[System.Serializable]
public class VehicleFactory
{
    #region Custom Data

    [System.Serializable]
    public class VehicleData
    {
        public GameObject   vehicleMesh;
        public Animator     vehicleAnimator;
        public RiderPose    riderPose;
        public Sprite       vehiclePP;

        [Space(5.0f)]
        public Vector3      offsetPositionForPlayerMesh;
        
    }

    #endregion

    #region Public Variables

    public string vehicleTag;
    public VehicleData[] vehicleDatas;

    #endregion

    #region Configuretion

    private bool IsValidVehicleIndex(int t_VehickeIndex) {

        if (t_VehickeIndex >= 0 && t_VehickeIndex < vehicleDatas.Length)
            return true;

        Debug.Log("Invalid CharacterIndex : " + t_VehickeIndex + ", Should be between [0," + vehicleDatas.Length + ")");
        return false;
    }

    private string GetPrefKeyForSelectedVehicle() {

        return "PrefKey_" + vehicleTag + "_SelectionIndex";
    }

    #endregion

    #region Public Variables

    public int GetSelectedVehicleIndex() {

        return PlayerPrefs.GetInt(GetPrefKeyForSelectedVehicle(), 0);
    }

    public void SetSelectedVehicleIndex(int t_CharacterIndex) {

        if (IsValidVehicleIndex(t_CharacterIndex))
            PlayerPrefs.SetInt(GetPrefKeyForSelectedVehicle(), t_CharacterIndex);
    }

    public void GoToNextVehicle() {

        int t_NewCharacterIndex = GetSelectedVehicleIndex() + 1;

        if (t_NewCharacterIndex >= vehicleDatas.Length) {

            t_NewCharacterIndex = 0;
        }

        PlayerPrefs.SetInt(GetPrefKeyForSelectedVehicle(), t_NewCharacterIndex);
    }

    public void GoPreviousVehicle() {

        int t_NewCharacterIndex = GetSelectedVehicleIndex() - 1;

        if (t_NewCharacterIndex < 0)
        {

            t_NewCharacterIndex = vehicleDatas.Length - 1;
        }

        PlayerPrefs.SetInt(GetPrefKeyForSelectedVehicle(), t_NewCharacterIndex);
    }

    public void EnableCurrentVehicle(Transform t_CharacterTransformReference = null) {

        int t_SelectedCharacterIndex = GetSelectedVehicleIndex();
        int t_NumberOfCharacter = vehicleDatas.Length;
        for (int i = 0; i < t_NumberOfCharacter; i++) {

            if (i == t_SelectedCharacterIndex)
            {
                vehicleDatas[i].vehicleMesh.SetActive(true);
                if(t_CharacterTransformReference != null){
                    t_CharacterTransformReference.localPosition = vehicleDatas[i].offsetPositionForPlayerMesh;
                }
            }
            else
            {
                vehicleDatas[i].vehicleMesh.SetActive(false);
            }
        }
    }

    public GameObject GetSelectedVehicleMesh() {

        return GetVehicleMesh(GetSelectedVehicleIndex());
    }

    public GameObject GetVehicleMesh(int t_CharacterIndex) {

        if (IsValidVehicleIndex(t_CharacterIndex))
            return vehicleDatas[t_CharacterIndex].vehicleMesh;

        return null;
    }

    public Animator GetSelectedVehicleAnimator() {

        return GetVehicleAnimator(GetSelectedVehicleIndex());
    }

    public Animator GetVehicleAnimator(int t_CharacterIndex)
    {

        if (IsValidVehicleIndex(t_CharacterIndex))
            return vehicleDatas[t_CharacterIndex].vehicleAnimator;

        return null;
    }

    public RiderPose GetSelectedVehiclePose() {

        return GetVehiclePose(GetSelectedVehicleIndex());
    }

    public RiderPose GetVehiclePose(int t_CharacterIndex)
    {

        if (IsValidVehicleIndex(t_CharacterIndex))
            return vehicleDatas[t_CharacterIndex].riderPose;

        return RiderPose.standing;
    }

    public Sprite GetSelectedVehiclePP(){

        return GetVehiclePP(GetSelectedVehicleIndex());
    }

    public Sprite GetVehiclePP(int t_CharacterIndex){

        if(IsValidVehicleIndex(t_CharacterIndex))
            return vehicleDatas[t_CharacterIndex].vehiclePP;

        return null;
    }

    #endregion
}
