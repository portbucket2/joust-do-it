﻿using UnityEngine;

[System.Serializable]
public class CharacterFactory
{
    #region Custom Data

    [System.Serializable]
    public class CharacterData
    {
        public Transform    characterHandPoint;
        public GameObject   characterMesh;
        public Animator     characterAnimator;
        public Sprite       characterPP;
        

        [Space(5.0f)]
        public Rigidbody[] rigidbodyReferencesForRagdoll;
    }

    #endregion

    #region Public Variables

    public string characterTag;
    public CharacterData[] characterDatas;

    #endregion

    #region Configuretion

    private bool IsCharacterIndexValid(int t_CharacterIndex) {

        if (t_CharacterIndex >= 0 && t_CharacterIndex < characterDatas.Length)
            return true;

        Debug.Log("Invalid CharacterIndex : " + t_CharacterIndex + ", Should be between [0," + characterDatas.Length + ")");
        return false;
    }

    private string GetPrefKeyForSelectedCharacter() {

        return "PrefKey_" + characterTag + "_SelectionIndex";
    }

    #endregion

    #region Public Variables

    public int GetSelectedCharacterIndex() {

        return PlayerPrefs.GetInt(GetPrefKeyForSelectedCharacter(), 0);
    }

    public void SetSelectedCharacterIndex(int t_CharacterIndex) {

        if (IsCharacterIndexValid(t_CharacterIndex))
            PlayerPrefs.SetInt(GetPrefKeyForSelectedCharacter(), t_CharacterIndex);
    }

    public void GoToNextCharacter() {

        int t_NewCharacterIndex = GetSelectedCharacterIndex() + 1;

        if (t_NewCharacterIndex >= characterDatas.Length) {

            t_NewCharacterIndex = 0;
        }

        PlayerPrefs.SetInt(GetPrefKeyForSelectedCharacter(), t_NewCharacterIndex);
    }

    public void GoPreviousCharacter() {

        int t_NewCharacterIndex = GetSelectedCharacterIndex() - 1;

        if (t_NewCharacterIndex < 0)
        {

            t_NewCharacterIndex = characterDatas.Length - 1;
        }

        PlayerPrefs.SetInt(GetPrefKeyForSelectedCharacter(), t_NewCharacterIndex);
    }

    public void EnableCurrentCharacter() {

        int t_SelectedCharacterIndex = GetSelectedCharacterIndex();
        int t_NumberOfCharacter = characterDatas.Length;
        for (int i = 0; i < t_NumberOfCharacter; i++) {

            if (i == t_SelectedCharacterIndex)
            {
                characterDatas[i].characterMesh.SetActive(true);
            }
            else
            {
                characterDatas[i].characterMesh.SetActive(false);
            }
        }
    }

    public Transform GetCurrentGrabingPoint()
    {
        return GetGrabingPoint(GetSelectedCharacterIndex());
    }

    public Transform GetGrabingPoint(int t_CharacterIndex) {

        if (IsCharacterIndexValid(t_CharacterIndex))
            return characterDatas[t_CharacterIndex].characterHandPoint;

        return null;
    }

    public GameObject GetSelectedCharacterMesh() {

        return GetCharacterMesh(GetSelectedCharacterIndex());
    }

    public GameObject GetCharacterMesh(int t_CharacterIndex) {

        if (IsCharacterIndexValid(t_CharacterIndex))
            return characterDatas[t_CharacterIndex].characterMesh;

        return null;
    }

    public Animator GetSelectedCharacterAnimator() {

        return GetCharacterAnimator(GetSelectedCharacterIndex());
    }

    public Animator GetCharacterAnimator(int t_CharacterIndex)
    {

        if (IsCharacterIndexValid(t_CharacterIndex))
            return characterDatas[t_CharacterIndex].characterAnimator;

        return null;
    }

    public Sprite GetSelectedCharacterPP(){

        return GetCharacterPP(GetSelectedCharacterIndex());
    }

    public Sprite GetCharacterPP(int t_CharacterIndex){

        if(IsCharacterIndexValid(t_CharacterIndex))
            return characterDatas[t_CharacterIndex].characterPP;

        return null;
    }

    public Rigidbody[] GetSelectedCharacterRagdollRigidbody() {

        return GetCharacterRagdollRigidbody(GetSelectedCharacterIndex());
    }

    public Rigidbody[] GetCharacterRagdollRigidbody(int t_CharacterIndex)
    {

        if (IsCharacterIndexValid(t_CharacterIndex))
            return characterDatas[t_CharacterIndex].rigidbodyReferencesForRagdoll;

        return null;
    }

    #endregion
}
