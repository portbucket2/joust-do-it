﻿
using UnityEngine;

using com.faithstudio.Gameplay;

public class PlayerController : MonoBehaviour
{

    #region Public Variables

    public Transform cameraPositionWhenRunning;
    public SkillTree playerSkillTree;

    [Space(5.0f)]
    public JoustingKnightController joustingKnightControllerReference;

    #endregion

    #region Private Variables

    private Transform m_TransformReference;

    #endregion

    #region Mono Behaviour

    private void Awake()
    {
        m_TransformReference = transform;
    }

    #endregion

    #region Public Variables

    public void PreProcess(
        float           t_Health,
        float           t_AttackPower,
        float           t_AttackPowerOfOpponent,
        Transform       t_EnemyTransformReference) {

        joustingKnightControllerReference.PreProcess(
            t_Health,
            t_AttackPower,
            t_AttackPowerOfOpponent,
            m_TransformReference,
            t_EnemyTransformReference,
            delegate{

                CharacterExpressionController.Instance.ShowExpression(
                                    0.5f,
                                    new Vector3(0f,6.25f,2.5f),
                                    GameplayController.Instance.enemyControllerReference.transform,
                                    GameplayController.Instance.expressionForSad[Random.Range(0, GameplayController.Instance.expressionForSad.Length)],
                                    "Damn You!!!!",
                                    Color.white
                                );
            },
            ShowExpressionForLosingHealth
            ,
            delegate{

                HapticFeedbackController.Instance.TapPeekVibrate();
                GameplayController.Instance.cameraMovementControllerReference.FocusCameraWithOrigin(
                                                    joustingKnightControllerReference.GetRagdollBodyPartReference()[0],
                                                    joustingKnightControllerReference.GetRagdollBodyPartReference(),
                                                    false,
                                                    Vector3.up * 2.5f,
                                                    new Vector3(10f, 5, 10f),
                                                    0.1f,
                                                    0.5f,
                                                    1f,
                                                    GameplayController.Instance.cameraAngulerRotationOnJoustingAttacking
                                                );
            },
            UIStateController.Instance.UpdatePlayerHealth);
    }

    public void PostProcess() { 
    

    }

    public void ShowExpressionForLosingHealth(float t_AmountOfHealthReduce){

        HapticFeedbackController.Instance.TapNopeVibrate();
        CharacterExpressionController.Instance.ShowExpression(
                                    0.5f,
                                    new Vector3(0f,6.25f,-5f),
                                    transform,
                                    GameplayController.Instance.expressionForLosingHealth,
                                    "-" + t_AmountOfHealthReduce.ToString("F0"),
                                    Color.red
                                );
    }

    public void SetOnHitMultiplierForPlayer(float t_ValueOfAccuracy){

        joustingKnightControllerReference.SetOnHitMultiplier(GameplayController.Instance.enemyControllerReference.GetAttackPerformance(
            GameplayController.Instance.levelManagerReference.GetLevelProgression(), 
            t_ValueOfAccuracy * GameplayController.Instance.GetInterpolatedVelocityWhenAimingStart()));
    }



    #endregion

    #region Public Callback :   Player State

    public float GetPlayerHealth()
    {
        return playerSkillTree.GetCurrentStatOfSkill(0);
    }

    public float GetPlayerAttackPower()
    {
        return playerSkillTree.GetCurrentStatOfSkill(1);
    }

    public float GetCoinEarnMultiplier() {

        return playerSkillTree.GetCurrentStatOfSkill(2);
    }

    #endregion

    #region  Public Callback    :   Player Upgrade

    public double GetUpgradeCostForHealth(){

        return playerSkillTree.GetUpgradeCostForNextLevel(0);
    }

    public double GetUpgradeCostForAttackPower(){

        return playerSkillTree.GetUpgradeCostForNextLevel(1);
    }

    public double GetUpgradeCostForCoinEarnMultiplier(){

        return playerSkillTree.GetUpgradeCostForNextLevel(2);
    }

    public void UpgradeHealth(){

        playerSkillTree.UpgradeSkill(0);
    }

    public void UpgradeAttackPower(){

        playerSkillTree.UpgradeSkill(1);
    }

    public void UpgradeCoinEarnMultiplier(){
        
        playerSkillTree.UpgradeSkill(2);
    }

    #endregion
}
