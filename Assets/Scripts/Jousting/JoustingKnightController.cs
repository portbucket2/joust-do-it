﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
using com.alphapotato.Gameplay;

[RequireComponent(typeof(CharacterMovementController))]
public class JoustingKnightController : MonoBehaviour
{

    #region Public Variables

#if UNITY_EDITOR

    [HideInInspector]
    public bool knightCharacterSettingFoldout;
    [HideInInspector]
    public bool knightHorseSettingsFoldout;

#endif

    public delegate void DataTypeOfOnPreProcess();
    public delegate void DataTypeOfOnPostProcess();

    public DataTypeOfOnPreProcess OnStartDuelling;
    public DataTypeOfOnPostProcess OnPostProcess;

    public bool isSetToGodMode;
    public bool isKnockedByOneHit;

    [Space(5.0f)]
    public Transform targetedPosition;

    [Header("Reference      :   HitPoint")]
    [Space(5.0f)]
    public Transform lanceHitPositionForOpponent;

    [Space(5.0f)]
    [Header("Reference      :   Lance")]
    public string opponentLanceTag;
    public Transform lanceTopPosition;
    public Transform lanceContainer;
    public Rigidbody lanceContainerRigidbody;
    public ItemFactory weaponFactory;

    [Header("Reference      :   Knight")]
    public Transform knightTransformReference;
    public ForceMode forceModeOnImpact;
    [Range(0f, 1000f)]
    public float strenghOnImpact;

    [Space(5.0f)]
    public CharacterFactory knightFactory;
    public VehicleFactory horseFactory;

    #endregion

    #region Private Variables

    private bool m_IsLanceRotatingToInitialRotation;
    private bool m_IsKnightRotatingToInitialRotation;
    private bool m_IsControllerForKnightSpeedAdjustmentRunning;

    private float m_RotationSpeedForLanceAiming = 0.1f;
    private float m_RotationSpeedForKnightFacing;

    private float m_OnHitMultiplier;
    private float m_CurrentHealthOfKnight;
    private float m_MaxHealthOfKnight;
    private float m_AttackPowerOfKnight;
    private float m_AttackPowerOfOpponentKnight;

    private CharacterMovementController m_CharacterMovementControllerReference;
    private TimeController m_TimeManagerReference;

    private Transform m_TransformReferenceOfOpponentKnight;
    private Transform m_TransformReferenceOfKnight;
    private Transform m_TransformReferenceForLanceTarget;
    private Transform m_TransformReferenceForKnightFacing;
    private Transform m_TransformReferenceForLanceGrabingPoint;

    private Vector3 m_InitialLocalPositionOfKnight;
    private Vector3 m_InitialLocalPositionOfLance;

    private Quaternion m_InitialRotationOfLance;
    private Quaternion m_InitialRotationForKnight;

    private UnityAction OnKnightZeroHit;
    private UnityAction<float> OnKnightHit;
    private UnityAction OnKnightDied;
    private UnityAction<float, float, float> OnUpdatingUIForHealth;
    private UnityAction<double> OnUpdateUIForCoinEarn;
    #endregion

    #region Mono Behaviour

#if UNITY_EDITOR

    private void OnEnable()
    {
        if (m_CharacterMovementControllerReference == null)
            m_CharacterMovementControllerReference = gameObject.GetComponent<CharacterMovementController>();
    }

#endif

    private void Awake()
    {
        //Random Pick Is Now Off
        //knightFactory.SetSelectedCharacterIndex(Random.Range(0, knightFactory.characterDatas.Length));
        knightFactory.EnableCurrentCharacter();
        horseFactory.EnableCurrentVehicle(knightTransformReference);
        DisableRagdoll();

        m_TransformReferenceForLanceGrabingPoint = knightFactory.GetCurrentGrabingPoint();
        m_InitialLocalPositionOfLance   = m_TransformReferenceForLanceGrabingPoint.position;
        m_InitialLocalPositionOfKnight  = knightTransformReference.localPosition;

        lanceContainer.position         = m_InitialLocalPositionOfLance;

        m_InitialRotationOfLance = lanceContainer.rotation;
        m_InitialRotationForKnight = knightTransformReference.rotation;

        if (m_CharacterMovementControllerReference == null)
            m_CharacterMovementControllerReference = gameObject.GetComponent<CharacterMovementController>();

        ResetKnightStateToDefault();
    }

    private void Start(){

        m_TimeManagerReference = TimeController.Instance;
    }

    private void Update()
    {
        ControllerForAimingLance();
        ControllerForFacingKnight();

    }

    #endregion

    #region Configuretion

    private void EnableAndDisableMonoBehaviour(bool t_IsEnable = true)
    {
        enabled = t_IsEnable;
    }

    private void ResetKnightStateToDefault(){

        RotateKnightToATarget();
        RotateLanceToATarget(null,0.1f);
    }

    private void SwitchAnimationForKnight(string t_AnimationKey)
    {

        knightFactory.GetSelectedCharacterAnimator().SetTrigger(t_AnimationKey);

    }

    private void SwitchAnimationForHorse(string t_AnimationKey)
    {
        horseFactory.GetSelectedVehicleAnimator().SetTrigger(t_AnimationKey);

    }

    private void ControllerForAimingLance()
    {

        float t_TimeScale = m_TimeManagerReference.GetAbsoluteTimeScale();

        Vector3 t_ModifiedPosition = Vector3.Lerp(
                lanceContainer.position,
                m_TransformReferenceForLanceGrabingPoint.position,
                m_RotationSpeedForLanceAiming * t_TimeScale
            );
        lanceContainer.position = t_ModifiedPosition;

        Quaternion t_LookRotation = m_IsLanceRotatingToInitialRotation ? m_InitialRotationOfLance : Quaternion.LookRotation(m_TransformReferenceForLanceTarget.position - lanceContainer.position);
        Quaternion t_ModifiedRotation = Quaternion.Slerp(
                lanceContainer.rotation,
                t_LookRotation,
                m_RotationSpeedForLanceAiming * t_TimeScale
            );
        lanceContainer.rotation = t_ModifiedRotation;
    }

    private void ControllerForFacingKnight()
    {

        float t_TimeScale = m_TimeManagerReference.GetAbsoluteTimeScale();
        Quaternion t_LookRotation = m_IsKnightRotatingToInitialRotation ? m_InitialRotationForKnight : Quaternion.LookRotation(m_TransformReferenceForKnightFacing.position - knightTransformReference.position);

        Quaternion t_ModifiedRotation = Quaternion.Slerp(
                knightTransformReference.rotation,
                t_LookRotation,
                m_RotationSpeedForKnightFacing * t_TimeScale
            );
        knightTransformReference.rotation = t_ModifiedRotation;
    }

    private IEnumerator ControllerForKnightAnimationSpeedAdjustment()
    {

        float t_CycleLength = 0.1f;
        WaitForSeconds t_CycleDelay = new WaitForSeconds(t_CycleLength);

        float t_CurrentSpeed;
        Animator t_KnightAnimatorReference = knightFactory.GetSelectedCharacterAnimator();

        while (m_IsControllerForKnightSpeedAdjustmentRunning)
        {

            t_CurrentSpeed = m_CharacterMovementControllerReference.GetCurrentInterpolatedVelocity();
            t_KnightAnimatorReference.speed = Mathf.Clamp(t_CurrentSpeed, 1f, 2f);

            yield return t_CycleDelay;
        }

        t_KnightAnimatorReference.speed = 1f;

        StopCoroutine(ControllerForKnightAnimationSpeedAdjustment());
    }

    #endregion

    #region Public Callback

    public float GetCurrentHealthOfKnight()
    {

        return m_CurrentHealthOfKnight;
    }

    public float GetMaxHealthOfKnight()
    {

        return m_MaxHealthOfKnight;
    }

    public float GetAttackPowerOfKnight()
    {

        return m_AttackPowerOfKnight;
    }

    public List<Transform> GetRagdollBodyPartReference()
    {

        List<Transform> t_Result = new List<Transform>();
        Rigidbody[] t_RigidbodyReference = knightFactory.GetSelectedCharacterRagdollRigidbody();
        int t_NumberOfRigidbodyReference = t_RigidbodyReference.Length;
        for (int i = 0; i < t_NumberOfRigidbodyReference; i++)
        {

            t_Result.Add(t_RigidbodyReference[i].transform);
        }

        return t_Result;
    }

    public void PreProcess(
        float t_Health,
        float t_AttackPower,
        float t_AttackPowerOfOpponent,
        Transform t_TransformReferenceOfKnight,
        Transform t_TransformReferenceOfKnightOpponent,
        UnityAction OnKnightZeroHit = null,
        UnityAction<float> OnKnightHit = null,
        UnityAction OnKnightDied = null,
        UnityAction<float, float, float> OnUpdatingUIForHealth = null,
        UnityAction<double> OnUpdateUIForCoinEarn = null)
    {

        m_CurrentHealthOfKnight = t_Health;
        m_MaxHealthOfKnight = t_Health;
        m_AttackPowerOfKnight = t_AttackPower;
        m_AttackPowerOfOpponentKnight = t_AttackPowerOfOpponent;

        m_TransformReferenceOfKnight = t_TransformReferenceOfKnight;
        m_TransformReferenceOfOpponentKnight = t_TransformReferenceOfKnightOpponent;
        m_TransformReferenceForLanceGrabingPoint = knightFactory.GetCurrentGrabingPoint();
        
        this.OnKnightZeroHit        = OnKnightZeroHit;
        this.OnKnightHit            = OnKnightHit;
        this.OnKnightDied           = OnKnightDied;
        this.OnUpdatingUIForHealth  = OnUpdatingUIForHealth;
        this.OnUpdateUIForCoinEarn  = OnUpdateUIForCoinEarn;
    }

    public void PostProcess()
    {

        m_CharacterMovementControllerReference.StopMove(true);
        m_IsControllerForKnightSpeedAdjustmentRunning = false;

        DisableRagdoll();
        ShowIdleAnimation();

        knightTransformReference.SetParent(transform);
        knightTransformReference.localPosition = m_InitialLocalPositionOfKnight;
        knightTransformReference.rotation = m_InitialRotationForKnight;

        lanceContainer.SetParent(transform);
        lanceContainer.position = m_InitialLocalPositionOfLance;
        lanceContainer.rotation = m_InitialRotationOfLance;

        OnPostProcess?.Invoke();

        ResetKnightStateToDefault();
    }

    public void StartRunningForTheDuel()
    {

        m_OnHitMultiplier = 1f;

        OnStartDuelling?.Invoke();
        m_CharacterMovementControllerReference.Move(
                    targetedPosition,
                    m_TransformReferenceOfKnight,
                    false,
                    0,
                    0,
                    0,
                    0,
                    delegate
                    {

                        ShowIdleAnimation();
                    });

        ShowRidingAnimation();

        RotateLanceToATarget(m_TransformReferenceOfOpponentKnight);
        RotateKnightToATarget();

        if (!m_IsControllerForKnightSpeedAdjustmentRunning)
        {
            m_IsControllerForKnightSpeedAdjustmentRunning = true;
            StartCoroutine(ControllerForKnightAnimationSpeedAdjustment());
        }

    }

    public void EnableRagdoll(float t_ForceAmount = 0f, Vector3 t_ForceDirection = new Vector3(), ForceMode t_ForceMode = ForceMode.Impulse)
    {

        knightFactory.GetSelectedCharacterAnimator().enabled = false;

        Rigidbody[] t_RagdollRigidbodyReference = knightFactory.GetSelectedCharacterRagdollRigidbody();
        int t_NumberOfRagdollRigidbodyReference = t_RagdollRigidbodyReference.Length;

        for (int i = 0; i < t_NumberOfRagdollRigidbodyReference; i++)
        {

            t_RagdollRigidbodyReference[i].isKinematic = false;
            t_RagdollRigidbodyReference[i].detectCollisions = true;

            if (t_ForceAmount > 0)
            {

                t_RagdollRigidbodyReference[i].AddForce(
                        t_ForceAmount * t_ForceDirection,
                        t_ForceMode
                    );

                t_RagdollRigidbodyReference[i].AddTorque(
                        t_ForceAmount * t_ForceDirection,
                        t_ForceMode
                    );
            }
        }
    }

    public void DisableRagdoll()
    {

        knightFactory.GetSelectedCharacterAnimator().enabled = true;

        Rigidbody[] t_RagdollRigidbodyReference = knightFactory.GetSelectedCharacterRagdollRigidbody();
        int t_NumberOfRagdollRigidbodyReference = t_RagdollRigidbodyReference.Length;

        for (int i = 0; i < t_NumberOfRagdollRigidbodyReference; i++)
        {
            t_RagdollRigidbodyReference[i].isKinematic = true;
            t_RagdollRigidbodyReference[i].detectCollisions = false;
        }
    }

    
    public void SetOnHitMultiplier(float t_OnHitMultiplier){

        m_OnHitMultiplier = t_OnHitMultiplier;
        Debug.Log(gameObject.name + " : OnHitMultipler = " + m_OnHitMultiplier);
    }

    public void DeductKnightHealth()
    {
        //Debug.Log(gameObject.name + " : LoseHealth = " + m_CurrentHealthOfKnight + ", WithOnHitMultiplier = " + m_OnHitMultiplier);
        
        float t_AbsoluteDamage  = m_AttackPowerOfOpponentKnight * m_OnHitMultiplier;

        if(isKnockedByOneHit || (t_AbsoluteDamage > m_CurrentHealthOfKnight))
            t_AbsoluteDamage = m_CurrentHealthOfKnight;
    
        m_CurrentHealthOfKnight -= t_AbsoluteDamage;
        
        if(m_CurrentHealthOfKnight < 1){
            t_AbsoluteDamage        += m_CurrentHealthOfKnight;
            m_CurrentHealthOfKnight = 0;
        }

        if(m_OnHitMultiplier == 0)
            OnKnightZeroHit?.Invoke();
        else if(m_CurrentHealthOfKnight <= 0)
            OnKnightDied?.Invoke();
        else
            OnKnightHit?.Invoke(t_AbsoluteDamage);
        
        OnUpdateUIForCoinEarn?.Invoke(t_AbsoluteDamage);
        OnUpdatingUIForHealth?.Invoke(m_CurrentHealthOfKnight, m_MaxHealthOfKnight, t_AbsoluteDamage);
    }

    public void ExecuteOnHitImpact()
    {

        EnableAndDisableMonoBehaviour(false);

        m_CharacterMovementControllerReference.StopMove();

        knightTransformReference.SetParent(null);
        lanceContainer.SetParent(null);

        lanceContainerRigidbody.constraints = RigidbodyConstraints.None;
        lanceContainerRigidbody.useGravity = true;
        EnableRagdoll();

    }

    public void RotateLanceToATarget(Transform t_LanceTargetReference = null, float t_RotationSpeedForLanceAiming = 0.01f)
    {

        if (m_TimeManagerReference == null)
            m_TimeManagerReference = TimeController.Instance;


        if (t_LanceTargetReference != null)
        {
            m_TransformReferenceForLanceTarget = t_LanceTargetReference;
            m_IsLanceRotatingToInitialRotation = false;
        }
        else
        {

            m_IsLanceRotatingToInitialRotation = true;
        }

        m_RotationSpeedForLanceAiming = t_RotationSpeedForLanceAiming;

        EnableAndDisableMonoBehaviour(true);
    }

    public void RotateKnightToATarget(Transform t_KnightFacingReference = null, float t_RotationSpeedForKnightFacing = 0.01f)
    {

        if (m_TimeManagerReference == null)
            m_TimeManagerReference = TimeController.Instance;


        if (t_KnightFacingReference != null)
        {
            m_TransformReferenceForKnightFacing = t_KnightFacingReference;
            m_IsKnightRotatingToInitialRotation = false;
        }
        else
        {

            m_IsKnightRotatingToInitialRotation = true;
        }

        m_RotationSpeedForKnightFacing = t_RotationSpeedForKnightFacing;

        EnableAndDisableMonoBehaviour(true);
    }


    public void ShowIdleAnimation()
    {

        SwitchAnimationForKnight("IDLE");
        SwitchAnimationForHorse("IDLE");
    }

    public void ShowRidingAnimation()
    {
        switch(horseFactory.GetSelectedVehiclePose()){
            case RiderPose.standing:
                horseFactory.GetSelectedVehicleAnimator().SetBool("IS_RIDE_STANDING",true);
                SwitchAnimationForKnight("RIDE_STAND");
            break;
            case RiderPose.sitting:
                horseFactory.GetSelectedVehicleAnimator().SetBool("IS_RIDE_STANDING",false);
                SwitchAnimationForKnight("RIDE_SIT");
            break;
        }
        
        SwitchAnimationForHorse("RIDE");
    }

    public void ShowOnHitAnimation()
    {

        SwitchAnimationForKnight("ON_HIT");
    }

    public void ShowOnKnockedAnimation()
    {
        SwitchAnimationForKnight("ON_KNOCKED");
        SwitchAnimationForHorse("IDLE");
    }

    #endregion
}
