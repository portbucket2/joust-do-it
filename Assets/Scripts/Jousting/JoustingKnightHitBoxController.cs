﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class JoustingKnightHitBoxController : MonoBehaviour
{

    #region Public Variables

    [Range(0f,0.5f)]
    public float sideForceBiasedness;
    public JoustingKnightController joustingKnightControllerReference;

    #endregion

    #region Private Variables

    private Rigidbody   m_RigidbodyReference;
    private bool        m_IsAllowedToGetHit;

    #endregion

    #region Mono Behaviour

    private void Awake()
    {
        m_RigidbodyReference                = gameObject.GetComponent<Rigidbody>();
        m_RigidbodyReference.useGravity     = false;
        m_RigidbodyReference.constraints    = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezePositionZ;
        joustingKnightControllerReference.OnStartDuelling += PreProcess;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (m_IsAllowedToGetHit && other.tag == joustingKnightControllerReference.opponentLanceTag)
        {

            if (!joustingKnightControllerReference.isSetToGodMode)
            {

                m_IsAllowedToGetHit = false;

                joustingKnightControllerReference.DeductKnightHealth();

                if (joustingKnightControllerReference.GetCurrentHealthOfKnight() <= 0) {

                    joustingKnightControllerReference.ExecuteOnHitImpact();

                    Vector3 t_Direction = Vector3.Normalize(joustingKnightControllerReference.knightTransformReference.position - other.transform.position);
                    t_Direction.y = Mathf.Abs(t_Direction.y + Random.Range(0.2f, 0.33f));
                    t_Direction.x += (t_Direction.x >= 0 ? 1 : -1) * (t_Direction.x * sideForceBiasedness);
                    t_Direction.x = Mathf.Clamp(t_Direction.x,-1,1);

                    m_RigidbodyReference.useGravity = true;
                    m_RigidbodyReference.constraints = RigidbodyConstraints.None;

                    joustingKnightControllerReference.EnableRagdoll(
                            joustingKnightControllerReference.strenghOnImpact,
                            t_Direction,
                            joustingKnightControllerReference.forceModeOnImpact
                        );

                    joustingKnightControllerReference.ShowOnKnockedAnimation();
                }
                else{

                    joustingKnightControllerReference.ShowOnHitAnimation();
                }
            }
            else
            {
                Debug.Log("GodModeEnabled : " + joustingKnightControllerReference.name);
            }
        }
    }

    #endregion

    #region Public Callback

    public void PreProcess()
    {
        m_IsAllowedToGetHit = true;
    }

    public void ResetData() {


    }

    #endregion

}
