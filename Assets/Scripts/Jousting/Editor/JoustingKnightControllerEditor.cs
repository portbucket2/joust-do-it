﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(JoustingKnightController))]
public class JoustingKnightControllerEditor : Editor
{

    #region Editor Parameter

    private JoustingKnightController JoustingKnightControllerReference;

    private Editor knightCharacterEditor;
    private Editor knightHorseEditor;

    #endregion

    #region Editor Methods

    private void OnEnable()
    {
        JoustingKnightControllerReference = (JoustingKnightController)target;
    }

    public override void OnInspectorGUI()
    {

        EditorGUILayout.BeginHorizontal();
        {
            GameObject t_MeshReference = JoustingKnightControllerReference.knightFactory.GetSelectedCharacterMesh();
            EditorGUILayout.LabelField("Character (" + (t_MeshReference != null? t_MeshReference.name : JoustingKnightControllerReference.knightFactory.GetSelectedCharacterIndex().ToString()) + ")"); ;

            if (GUILayout.Button("Previous"))
            {
                JoustingKnightControllerReference.knightFactory.GoPreviousCharacter();
                JoustingKnightControllerReference.knightFactory.EnableCurrentCharacter();
                JoustingKnightControllerReference.lanceContainer.position = JoustingKnightControllerReference.knightFactory.GetCurrentGrabingPoint().position;
            }

            if (GUILayout.Button("Next")) {

                JoustingKnightControllerReference.knightFactory.GoToNextCharacter();
                JoustingKnightControllerReference.knightFactory.EnableCurrentCharacter();
                JoustingKnightControllerReference.lanceContainer.position = JoustingKnightControllerReference.knightFactory.GetCurrentGrabingPoint().position;
            }
        }
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        {

            GameObject t_MeshReference = JoustingKnightControllerReference.horseFactory.GetSelectedVehicleMesh();
            EditorGUILayout.LabelField("Vehicle (" + (t_MeshReference != null? t_MeshReference.name : JoustingKnightControllerReference.horseFactory.GetSelectedVehicleIndex().ToString()) + ")"); ;
          
            if (GUILayout.Button("Previous"))
            {

                JoustingKnightControllerReference.horseFactory.GoPreviousVehicle();
                JoustingKnightControllerReference.horseFactory.EnableCurrentVehicle(JoustingKnightControllerReference.knightTransformReference);
                JoustingKnightControllerReference.lanceContainer.position = JoustingKnightControllerReference.knightFactory.GetCurrentGrabingPoint().position;
            }

            if (GUILayout.Button("Next")) {

                JoustingKnightControllerReference.horseFactory.GoToNextVehicle();
                JoustingKnightControllerReference.horseFactory.EnableCurrentVehicle(JoustingKnightControllerReference.knightTransformReference);
                JoustingKnightControllerReference.lanceContainer.position = JoustingKnightControllerReference.knightFactory.GetCurrentGrabingPoint().position;
            }
        }
        EditorGUILayout.EndHorizontal();

        if (GUILayout.Button("Configure Ragdoll")) {

            ConfigureRagdoll();
        }

        EditorGUILayout.Space();
        base.OnInspectorGUI();

        //EditorGUILayout.Space();
        //DrawCustomSettings(
        //    JoustingKnightControllerReference.knightFactory,
        //    null,
        //    ref JoustingKnightControllerReference.knightCharacterSettingFoldout,
        //    ref knightCharacterEditor);
    }
    #endregion

    #region Custom Methods

    private void ConfigureRagdoll() {

        int t_NumberOfAvailableKnight = JoustingKnightControllerReference.knightFactory.characterDatas.Length;
        for (int i = 0; i < t_NumberOfAvailableKnight; i++) {

            Transform t_KnightTransformReference = JoustingKnightControllerReference.knightFactory.characterDatas[i].characterMesh.transform;
            JoustingKnightControllerReference.knightFactory.characterDatas[i].rigidbodyReferencesForRagdoll = t_KnightTransformReference.GetComponentsInChildren<Rigidbody>();

        }
    }


    private void DrawCustomSettings(
        Object characterFactory, 
        System.Action OnSettingChanged,
        ref bool foldout, 
        ref Editor editor) {

        if (characterFactory != null) {

            using (var check = new EditorGUI.ChangeCheckScope()) {

                foldout = EditorGUILayout.InspectorTitlebar(foldout, characterFactory);

                if (foldout)
                {

                    CreateCachedEditor(characterFactory, null, ref editor);
                    editor.OnInspectorGUI();

                    if (check.changed)
                    {

                        if (OnSettingChanged != null)
                        {

                            OnSettingChanged.Invoke();
                        }
                    }
                }
            }
        }
    }

    #endregion

}
