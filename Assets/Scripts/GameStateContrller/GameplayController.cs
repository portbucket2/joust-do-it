﻿//Template
//#region Public Variables

//#endregion

//#region Private Variables

//#endregion

//#region Mono Behaviour

//#endregion

//#region Configuretion

//#endregion

//#region Public Callback

//#endregion

using UnityEngine;

using System.Collections;
using System.Collections.Generic;

using com.alphapotato.Gameplay;

using com.faithstudio.Gameplay;
using com.faithstudio.ScriptedParticle;
using com.faithstudio.Camera;
using com.faithstudio.SDK;

public class GameplayController : MonoBehaviour
{
    #region Public Variables

    public static GameplayController Instance;


    [Header("Reference      :   External")]
    public LevelManager levelManagerReference;
    public CameraShake cameraShakeReference;
    public CameraMovementController cameraMovementControllerReference;
    public CharacterMovementController playerMovementControllerReference;
    public PlayerController playerControllerReference;
    public EnemyController enemyControllerReference;
    public AimingController aimingControllerReference;
    public PowerBarController powerBarControllerReference;
    public CrowdController crowdControllerReference;

    [Space(5.0f)]
    [Header("Reference      :   Expression")]
    public Sprite   expressionForHit;
    public Sprite   expressionForLosingHealth;
    public Sprite[] expressionForCheer;
    public Sprite[] expressionForSad;

    [Space(5.0f)]
    [Header("Reference      :   ParticleSystem")]
    [Range(0f, 1f)]
    public float fxStartAtPoint = 0.25f;
    public ParticleSystem levelCompleteParticle;
    public ParticleController[] scriptedParticles;

    [Space(5.0f)]
    [Header("Reference      :   CameraPositionOnStrike")]
    public Transform cameraPositionOnInitial;
    public Transform[] cameraPositionsOnStrike;
    public AnimationCurve cameraAngulerRotationOnJoustingRunning;
    public AnimationCurve cameraAngulerRotationOnJoustingAttacking;

    [Space(5.0f)]
    [Header("Configuretion  :   Gameplay (BulletTime)")]
    [Range(0f, 100f)]
    public float distanceOnEnablingAimingMechanincs;

    [Space(5.0f)]
    [Range(0f, 1f)]
    public float timeScaleDuringBulletTime;
    [Range(1f, 10f)]
    public float durationForBulletTime;
    public AnimationCurve timeScaleBlendingForBulletTimeOnTransation;

    [Space(5.0f)]
    public string   messangeWhenEnteredBulletTime;
    public Color    colorOfTheMessageOnBulletTime;

    [Space(5.0f)]
    [Range(0.01f, 10f)]
    public float durationForRestoringTimeScale;
    public AnimationCurve timeScaleBlendingForRestoreOnTransation;

    [Space(5.0f)]
    [Header("Configuretion  :   UserInput")]
    public bool isPerformanceCalculateFromPowerBar;
    [Range(0f, 1f)]
    public float accelerationIncreaseOnTap;

    #endregion

    #region Private Variables

    private bool m_IsGameplayRunning;

    private bool m_IsValueIncreaseOnScriptedParticle;
    private bool m_IsVisualFXControllerRunning;

    private float m_InterpolatedVelocityOfPlayerWhenAimingStart;
    [SerializeField]
    private ParticleController[] m_WeaponGlace;

    #endregion

    #region Mono Behaviour

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {

            Destroy(gameObject);
        }
        ActivateEnvironmentBasedOnLevel();
    }

    private void Start()
    {
        UIStateController.Instance.AppearMainMenu();

    }

    #endregion

    #region Configuretion

    private void StartVisualFX()
    {

        if (!m_IsVisualFXControllerRunning)
        {
            m_IsValueIncreaseOnScriptedParticle = true;
            m_IsVisualFXControllerRunning = true;
            StartCoroutine(ControllerForVisualFX());
        }
    }

    private void StopVisualFX()
    {

        m_IsVisualFXControllerRunning = false;
        cameraShakeReference.StopCameraShake();
    }

    private IEnumerator ControllerForVisualFX()
    {
        WaitForEndOfFrame t_WaitForEndOfFrame = new WaitForEndOfFrame();

        m_WeaponGlace  = playerControllerReference.joustingKnightControllerReference.weaponFactory.GetSelectedItemGlaceOnDefault();
        int t_NumberOfWeaponGlace           = m_WeaponGlace.Length;

        int t_NumberOfScriptedParticle = scriptedParticles.Length;
        

        bool t_IsVXParticleStart = false;
        float t_CurrentValue;
        float t_RangeOfInterpolatedValue = 1f - fxStartAtPoint;

        while (m_IsVisualFXControllerRunning)
        {

            t_CurrentValue = GetCurrentInterpolatedVelocity();

            if (!t_IsVXParticleStart && t_CurrentValue >= fxStartAtPoint)
            {

                t_IsVXParticleStart = true;
                for (int i = 0; i < t_NumberOfScriptedParticle; i++)
                {
                    scriptedParticles[i].PlayParticle();
                }

                for(int j = 0 ; j < t_NumberOfWeaponGlace; j++){

                    m_WeaponGlace[j].PlayParticle();
                }

                cameraShakeReference.ShowCameraShake();
            }

            for (int i = 0; i < t_NumberOfScriptedParticle; i++)
            {

                if (m_IsValueIncreaseOnScriptedParticle)
                {
                    scriptedParticles[i].LerpOnPreset((t_CurrentValue - fxStartAtPoint) / t_RangeOfInterpolatedValue);
                }
                else
                {
                    scriptedParticles[i].LerpOnDefault(1f - t_CurrentValue);
                }
            }

            for(int j = 0 ; j < t_NumberOfWeaponGlace; j++){

                    if (m_IsValueIncreaseOnScriptedParticle)
                        m_WeaponGlace[j].LerpOnPreset((t_CurrentValue - fxStartAtPoint) / t_RangeOfInterpolatedValue);
                    else
                        m_WeaponGlace[j].LerpOnDefault(1f - t_CurrentValue);
                }

            yield return t_WaitForEndOfFrame;
        }

        for (int i = 0; i < t_NumberOfScriptedParticle; i++)
        {
            scriptedParticles[i].StopParticle(false);
        }

        for(int j = 0 ; j < t_NumberOfWeaponGlace; j++)
            m_WeaponGlace[j].StopParticle(false);

        StopCoroutine(ControllerForVisualFX());
    }

    private IEnumerator ControllerForPreProcess()
    {
        
        FacebookAnalyticsManager.Instance.FBALevelStart(levelManagerReference.GetCurrentLevel());
        HapticFeedbackController.Instance.EnableHapticFeedback();
        CoinEarningController.Instance.PreProcess(10, levelManagerReference.GetLevelProgression());

        bool  t_HasUserAimed;
        float t_PlayerHealth = playerControllerReference.GetPlayerHealth();
        float t_PlayerAttackPower = playerControllerReference.GetPlayerAttackPower();

        float t_OpponentHealth = enemyControllerReference.GetModifiedEnemyHealth(
                                    levelManagerReference.GetLevelProgression(),
                                    t_PlayerHealth);
        float t_OpponentAttackPower = enemyControllerReference.GetModifiedEnemyAttackPower(
                levelManagerReference.GetLevelProgression(),
                t_PlayerAttackPower
            );

        TutorialController.Instance.ShowTutorial(false);

        UIStateController.Instance.DisappearMainMenu();

        UIStateController.Instance.UpdatePlayerHealth(t_PlayerHealth,t_PlayerHealth);
        UIStateController.Instance.UpdateOpponentHealth(t_OpponentHealth, t_OpponentHealth);

        playerControllerReference.PreProcess(
            t_PlayerHealth,
            t_PlayerAttackPower,
            t_OpponentAttackPower,
            enemyControllerReference.joustingKnightControllerReference.lanceHitPositionForOpponent);
        //powerBarControllerReference.OnPassingTheAccuracyEvaluation += playerControllerReference.SetOnHitMultiplierForPlayer;
        aimingControllerReference.OnPassingTheAccuracyEvaluation += playerControllerReference.SetOnHitMultiplierForPlayer;

        enemyControllerReference.PreProcess(
            t_OpponentHealth,
            t_OpponentAttackPower,
            t_PlayerAttackPower,
            playerControllerReference.joustingKnightControllerReference.lanceHitPositionForOpponent);
        //powerBarControllerReference.OnPassingTheAccuracyEvaluation += enemyControllerReference.joustingKnightControllerReference.SetOnHitMultiplier;
        aimingControllerReference.OnPassingTheAccuracyEvaluation += enemyControllerReference.joustingKnightControllerReference.SetOnHitMultiplier;

        Transform playerTransformReference = playerControllerReference.transform;
        Transform enemyTransformReference = enemyControllerReference.transform;

        int t_RoundNumber = 1;
        while (!IsGameEnded())
        {
            UIStateController.Instance.UpdateRoundNumber(t_RoundNumber);
            UIStateController.Instance.AppearProgressBarForSpeedBooster();
            UIStateController.Instance.AppearSpeedBoosterButton();

            GlobalTouchController.Instance.EnableTouchController();

            WaitUntil t_WaitUntilThePlayersComeNearToEachOther = new WaitUntil(() =>
            {
                float t_CurrentDistance = Vector3.Distance(playerTransformReference.position, enemyTransformReference.position);
                if (t_CurrentDistance <= distanceOnEnablingAimingMechanincs)
                {
                    return true;
                }
                return false;
            });

            playerControllerReference.joustingKnightControllerReference.StartRunningForTheDuel();
            enemyControllerReference.joustingKnightControllerReference.StartRunningForTheDuel();

            StartVisualFX();

            CameraMovementController.Instance.FocusCameraWithOrigin(
                playerControllerReference.cameraPositionWhenRunning,
                new List<Transform> { playerControllerReference.joustingKnightControllerReference.lanceTopPosition, enemyControllerReference.joustingKnightControllerReference.lanceHitPositionForOpponent },
                true,
                new Vector3(0, 0f, 0),
                Vector3.zero,
                0f,
                0f,
                0f,
                cameraAngulerRotationOnJoustingRunning
            );

            yield return t_WaitUntilThePlayersComeNearToEachOther;

            m_IsValueIncreaseOnScriptedParticle = false;

            UIStateController.Instance.DisappearSpeedBoosterButton();
            UIStateController.Instance.DisappearProgressBarForSpeedBooster();
            
            //No Need
            //UIStateController.Instance.UIMessageDialougeController.ShowMessage(messangeWhenEnteredBulletTime, colorOfTheMessageOnBulletTime, 1f);
            
            TutorialController.Instance.HideTutorial();

            yield return new WaitForSeconds(1f);

            m_InterpolatedVelocityOfPlayerWhenAimingStart = GetCurrentInterpolatedVelocity();

            enemyControllerReference.joustingKnightControllerReference.RotateLanceToATarget(
                        playerControllerReference.joustingKnightControllerReference.knightTransformReference,
                        0.1f
                    );
            enemyControllerReference.joustingKnightControllerReference.RotateKnightToATarget(
                        playerControllerReference.joustingKnightControllerReference.knightTransformReference,
                        0.25f);

            UIStateController.Instance.AppearTimerForAimingBar(durationForBulletTime);
            t_HasUserAimed      = false;
            cameraShakeReference.StopCameraShake();
            aimingControllerReference.PreProcess(delegate{
                UIStateController.Instance.DisappearTimerForAimingBar();
                t_HasUserAimed = true;
                GlobalTouchController.Instance.DisableTouchController();

                enemyControllerReference.joustingKnightControllerReference.RotateLanceToATarget(
                        playerControllerReference.joustingKnightControllerReference.GetRagdollBodyPartReference()[0],
                        0.825f
                    );

                playerControllerReference.joustingKnightControllerReference.RotateLanceToATarget(
                        isPerformanceCalculateFromPowerBar ? enemyControllerReference.joustingKnightControllerReference.GetRagdollBodyPartReference()[0] : aimingControllerReference.finalCrosshairPoint,
                        0.825f
                    );
                

                playerControllerReference.joustingKnightControllerReference.RotateKnightToATarget(
                        enemyTransformReference,
                        0.325f
                    );
                
                TimeController.Instance.DoFastMotion(
                            true,
                            durationForRestoringTimeScale,
                            1f,
                            timeScaleBlendingForRestoreOnTransation,
                            delegate
                            {

                                TimeController.Instance.RestoreToInitialTimeScale();

                                playerControllerReference.joustingKnightControllerReference.RotateLanceToATarget();
                                playerControllerReference.joustingKnightControllerReference.RotateKnightToATarget();

                                enemyControllerReference.joustingKnightControllerReference.RotateLanceToATarget();
                                enemyControllerReference.joustingKnightControllerReference.RotateKnightToATarget();

                                StopVisualFX();

                                if (!IsGameEnded()){

                                    cameraMovementControllerReference.FocusCameraWithOrigin(
                                            cameraPositionsOnStrike[Random.Range(0, cameraPositionsOnStrike.Length)],
                                            new List<Transform>() { playerControllerReference.joustingKnightControllerReference.knightTransformReference, enemyControllerReference.joustingKnightControllerReference.knightTransformReference },
                                            false,
                                            Vector3.up * 2.5f,
                                            Vector3.zero,
                                            0.1f,
                                            0.5f,
                                            1f,
                                            cameraAngulerRotationOnJoustingAttacking
                                        );
                                }
                            }
                        );
            });
            
            TimeController.Instance.DoSlowMotion(
                true,
                durationForBulletTime,
                timeScaleDuringBulletTime,
                timeScaleBlendingForBulletTimeOnTransation,
                delegate{
                    
                    UIStateController.Instance.DisappearTimerForAimingBar();
                    if(!t_HasUserAimed){
                        
                        TimeController.Instance.RestoreToInitialTimeScale();

                                playerControllerReference.joustingKnightControllerReference.RotateLanceToATarget();
                                playerControllerReference.joustingKnightControllerReference.RotateKnightToATarget();

                                enemyControllerReference.joustingKnightControllerReference.RotateLanceToATarget(
                                    playerControllerReference.joustingKnightControllerReference.knightTransformReference,
                                    0.75f
                                );

                                enemyControllerReference.joustingKnightControllerReference.RotateKnightToATarget(
                                    playerControllerReference.joustingKnightControllerReference.knightTransformReference,
                                    0.25f
                                );

                                StopVisualFX();

                                CharacterExpressionController.Instance.ShowExpression(
                                    0.5f,
                                    new Vector3(0f,6.25f,2.5f),
                                    enemyTransformReference,
                                    expressionForCheer[Random.Range(0, expressionForCheer.Length)],
                                    "Loser !!",
                                    Color.white
                                );
                                
                    }
                });

            float t_Distance                    = Vector3.Distance(playerTransformReference.position, enemyTransformReference.position);
            float t_InterpolatedValueOnDistance = t_Distance / distanceOnEnablingAimingMechanincs;
            float t_AbsoluteDelay               = Mathf.Lerp(0.5f,1.5f, t_InterpolatedValueOnDistance);
            yield return new WaitForSeconds(t_AbsoluteDelay);

            if(!t_HasUserAimed){
                
                enemyControllerReference.joustingKnightControllerReference.RotateKnightToATarget();
                //powerBarControllerReference.HidePowerBar();
                aimingControllerReference.PostProcess();
                yield return new WaitForSeconds(0.75f);

                enemyControllerReference.joustingKnightControllerReference.RotateLanceToATarget();
            }

            yield return new WaitForSeconds(4f - t_AbsoluteDelay - (t_HasUserAimed ? 0f : 0.5f));

            if (!IsGameEnded())
            {
                playerControllerReference.joustingKnightControllerReference.PostProcess();
                enemyControllerReference.joustingKnightControllerReference.PostProcess();

                cameraMovementControllerReference.FocusCameraWithOrigin(
                    cameraPositionOnInitial,
                    new List<Transform>() { playerControllerReference.joustingKnightControllerReference.knightTransformReference },
                    true,
                    Vector3.forward * 5f,
                    Vector3.zero,
                    0.1f,
                    0.25f,
                    0.25f,
                    cameraAngulerRotationOnJoustingRunning
                );

                UIStateController.Instance.ShowTransationForRoundEnd(t_RoundNumber++);
                yield return new WaitForSeconds(3f);
                
            }else{
                
                Debug.Log("Game Ended");
                yield return new WaitForSeconds(5f);
            }
        }
        StopCoroutine(ControllerForPreProcess());

        if(IsPlayerWonTheGame()){
            levelCompleteParticle.Play();
            levelManagerReference.IncreaseLevel();
        }else{
            FacebookAnalyticsManager.Instance.FBALevelFailed(levelManagerReference.GetCurrentLevel());
        }
            
        UIStateController.Instance.AppearGameOverMenu(IsPlayerWonTheGame(),CoinEarningController.Instance.GetNumberOfCoinEarnedInThisLevel());
        CoinEarningController.Instance.PostProcess();
        HapticFeedbackController.Instance.DisableHapticFeedback();
    }

    #endregion

    #region Public Callback

    public void PreProcess()
    {

        if (!m_IsGameplayRunning)
        {

            m_IsGameplayRunning = true;
            StartCoroutine(ControllerForPreProcess());
        }
    }

    public void PostProcess()
    {


    }

    public bool IsGameEnded()
    {

        if (playerControllerReference.joustingKnightControllerReference.GetCurrentHealthOfKnight() <= 0 || enemyControllerReference.joustingKnightControllerReference.GetCurrentHealthOfKnight() <= 0)
            return true;

        return false;
    }

    public bool IsPlayerWonTheGame()
    {

        if (playerControllerReference.joustingKnightControllerReference.GetCurrentHealthOfKnight() > enemyControllerReference.joustingKnightControllerReference.GetCurrentHealthOfKnight())
            return true;

        return false;
    }

    public void ActivateEnvironmentBasedOnLevel(){

        int t_EnvironmentIndex = levelManagerReference.GetIndexOfEnvironmentForCurrentLevel();
        int t_NumberOfEnvironment = levelManagerReference.environmentReference.Length;
        for(int i = 0 ; i < t_NumberOfEnvironment; i++){

            if(i == t_EnvironmentIndex){

                levelManagerReference.environmentReference[i].SetActive(true);
            }else{
                levelManagerReference.environmentReference[i].SetActive(false);
            }
        }
        crowdControllerReference.SwitchToCrowdIndex(t_EnvironmentIndex);
        crowdControllerReference.ActivateCrowd();
    }

    public void OnButtonPressedForSpeedIncrease()
    {
        HapticFeedbackController.Instance.TapNopeVibrate();
        playerMovementControllerReference.IncreaseAcceleration(accelerationIncreaseOnTap);
    }

    public float GetCurrentInterpolatedVelocity()
    {
        return playerMovementControllerReference.GetCurrentInterpolatedVelocity();
    }

    public float GetInterpolatedVelocityWhenAimingStart(){

        return m_InterpolatedVelocityOfPlayerWhenAimingStart;
    }

    public void ShowCheerExpression(Transform t_ExpressionTarget, float t_Duration = 0.5f)
    {
        CharacterExpressionController.Instance.ShowEmoji(
                t_Duration,
                Vector3.up * 7.5f,
                t_ExpressionTarget,
                expressionForCheer[Random.Range(0, expressionForCheer.Length)]
            );
    }

    public void ShowSadExpression(Transform t_ExpressionTarget, float t_Duration = 0.5f)
    {

        CharacterExpressionController.Instance.ShowEmoji(
                t_Duration,
                Vector3.up * 7.5f,
                t_ExpressionTarget,
                expressionForSad[Random.Range(0, expressionForSad.Length)]
            );
    }

    #endregion

}
