﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(CrowdController))]
public class CrowdControllerEditor : Editor
{
    private CrowdController CrowdControllerReference;

    private void OnEnable()
    {
        CrowdControllerReference = (CrowdController)target;
    }

    public override void OnInspectorGUI()
    {

        serializedObject.Update();

        EditorGUILayout.BeginHorizontal();
        {
            EditorGUILayout.LabelField("CrowdIndex (" + CrowdControllerReference.GetCurrentCrowdGroupIndex() + ")");
            if (GUILayout.Button("<-")) {

                CrowdControllerReference.GoToPreviousCrowdIndex();
            }
            if (GUILayout.Button("->"))
            {
                CrowdControllerReference.GoToNextCrowdIndex();
            }
        }
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.Space();
        if (GUILayout.Button("Find Crowd")) {

            int t_NumberOfCrowdGroup = CrowdControllerReference.crowdGroups.Length;
            for (int i = 0; i < t_NumberOfCrowdGroup; i++) {

                if (CrowdControllerReference.crowdGroups[i].crowdGroupParent != null)
                {
                    CrowdControllerReference.crowdGroups[i].enlistedGroupForThisGroup = CrowdControllerReference.crowdGroups[i].crowdGroupParent.GetComponentsInChildren<CrowdBehaviour>();
                    int t_NumberOfEnlistedCrowd = CrowdControllerReference.crowdGroups[i].enlistedGroupForThisGroup.Length;
                    for (int j = 0; j < t_NumberOfEnlistedCrowd; j++) {

                        CrowdControllerReference.crowdGroups[i].enlistedGroupForThisGroup[j].ConfigRagdoll();
                    }
                }
                else {

                    Debug.LogError("Null Reference : CrowdParent was not assigned for CrowdGroup(" + i + ")");
                }
            }
        }

        EditorGUILayout.Space(5.0f);

        base.OnInspectorGUI();

        serializedObject.ApplyModifiedProperties();
    }
}
