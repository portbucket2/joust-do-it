﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(CrowdBehaviour))]
public class CrowdBehaviourEditor : Editor
{
    private CrowdBehaviour CrowdBehaviourReference;

    private void OnEnable()
    {
        CrowdBehaviourReference = (CrowdBehaviour)target;
    }

    public override void OnInspectorGUI()
    {

        EditorGUILayout.BeginHorizontal();
        {
            if (GUILayout.Button("RandomForce")) {

                CrowdBehaviourReference.EnableRagdoll();
                CrowdBehaviourReference.ApplyForceOnImpact(Random.onUnitSphere);
            }

            if (GUILayout.Button("Config Ragdoll"))
            {

                CrowdBehaviourReference.ConfigRagdoll();

            }
        }
        EditorGUILayout.EndHorizontal();


        

        base.OnInspectorGUI();
    }
}
