﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using com.faithstudio.Gameplay;
using com.alphapotato.Gameplay;

public class CrowdBehaviour : MonoBehaviour
{

    #region Public Variables

    public Transform    crowdMeshTransformReference;
    public Animator     crowdAnimatorReference;

    [Space(10.0f)]
    [Range(0f,1000f)]
    public float        reactionForceOnImpact;
    public ForceMode    forceModeOfReactionOnImpact;

    #endregion

    #region Private Variables

    private bool        m_IsAbleToRecieveImpact;

    //[HideInInspector]
    public Rigidbody    rigidbodyReference;
    //[HideInInspector]
    public Rigidbody[]  rigidbodyReferenceOfBones;

    #endregion

    #region Mono Behaviour

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Tag_Opponent" || collision.gameObject.tag == "Tag_Player") {

            EnableRagdoll();
            ApplyForceOnImpact(Vector3.Normalize(transform.position - collision.transform.position));
        }
    }

    #endregion

    #region Configuretion

    public void EnableRagdoll() {


        rigidbodyReference.isKinematic      = false;
        rigidbodyReference.detectCollisions = false;
        
        crowdAnimatorReference.enabled = false;

        int t_NumberOfRagdollBone = rigidbodyReferenceOfBones.Length;
        for (int i = 0; i < t_NumberOfRagdollBone; i++) {

            rigidbodyReferenceOfBones[i].isKinematic = false;
        }
    }

    private void DisableRagdoll()
    {
        int t_NumberOfRagdollBone = rigidbodyReferenceOfBones.Length;
        for (int i = 0; i < t_NumberOfRagdollBone; i++)
        {

            rigidbodyReferenceOfBones[i].isKinematic = true;
        }

        crowdAnimatorReference.enabled = true;

        if (rigidbodyReference == null)
            Debug.Log("Ami ### : " + gameObject.name);

        rigidbodyReference.isKinematic      = true;
        rigidbodyReference.detectCollisions = true;
    }

    private void OnForceImpact(){

        Debug.Log("Cinematic Getting Called!!");

        StartCoroutine(ControllerForCoinEarning(CrowdController.Instance.GetNumberOfCoinToBeRewardedForHittingCrowd()));
            
            List<Transform> t_TransformReferenceForKnightWhoLosed = GameplayController.Instance.IsPlayerWonTheGame() ? GameplayController.Instance.enemyControllerReference.joustingKnightControllerReference.GetRagdollBodyPartReference() : GameplayController.Instance.playerControllerReference.joustingKnightControllerReference.GetRagdollBodyPartReference();
            List<Transform> t_TransformReferencesForKnightAndCivilian = new List<Transform>(t_TransformReferenceForKnightWhoLosed);
            int t_NumberOfBone = rigidbodyReferenceOfBones.Length;
            for(int i = 0 ; i < t_NumberOfBone; i++){

                t_TransformReferencesForKnightAndCivilian.Add(rigidbodyReferenceOfBones[0].transform);
            }
            
            Vector3 t_CameraOffset = new Vector3(
                Random.Range(0f,1f) <= 0.5f ? Random.Range(5,10) : Random.Range(-10,-5),
                Random.Range(3,5),
                Random.Range(0f,1f) <= 0.5f ? Random.Range(5,10) : Random.Range(-10,-5)
            );

            GameplayController.Instance.cameraMovementControllerReference.FocusCameraWithOrigin(
                                                    t_TransformReferencesForKnightAndCivilian[0],
                                                    t_TransformReferencesForKnightAndCivilian,
                                                    false,
                                                    Vector3.up * 2.5f,
                                                    t_CameraOffset,
                                                    Random.Range(0.2f,0.3f),
                                                    0.5f,
                                                    1f,
                                                    GameplayController.Instance.cameraAngulerRotationOnJoustingAttacking
                                                );

            TimeController.Instance.DoSlowMotion(
                true,
                1.5f,
                0.1f,
                CrowdController.Instance.curveForSlowMotionOnHit,
                delegate{

                    TimeController.Instance.DoFastMotion(
                        true,
                        1.5f,
                        2,
                        CrowdController.Instance.curveForRestoreMotionOnHit,
                        delegate{

                            TimeController.Instance.RestoreToInitialTimeScale();
                        }
                    );

                    GameplayController.Instance.cameraMovementControllerReference.FocusCameraWithOrigin(
                                                    t_TransformReferenceForKnightWhoLosed[0],
                                                    t_TransformReferenceForKnightWhoLosed,
                                                    false,
                                                    Vector3.up * 2.5f,
                                                    new Vector3(-10,5,-10),
                                                    0.1f,
                                                    0.5f,
                                                    1f,
                                                    GameplayController.Instance.cameraAngulerRotationOnJoustingAttacking
                                                );
                }
            );
    }

    private IEnumerator ControllerForCoinEarning(int t_NumberOfCoinToBeAdded){

        float t_CycleLength = 0.5f;
        WaitForSecondsRealtime t_CycleDelay = new WaitForSecondsRealtime(t_CycleLength);

        while(t_NumberOfCoinToBeAdded > 0){

            CoinEarningController.Instance.AddCoinToUser(
                crowdMeshTransformReference.position + Vector3.up * 3,
                Vector3.zero,
                Vector3.zero,
                CrowdController.Instance.GetCrowdCoinEarnMultiplier(),
                true
            );
            t_NumberOfCoinToBeAdded--;
            yield return t_CycleLength;
        }

        StopCoroutine(ControllerForCoinEarning(0));
    }

    #endregion

    #region Public Callback

#if UNITY_EDITOR

    public void ConfigRagdoll() {

        rigidbodyReferenceOfBones = crowdMeshTransformReference.GetComponentsInChildren<Rigidbody>();

        Rigidbody t_RigidbodyReference = gameObject.GetComponent<Rigidbody>();
        if (t_RigidbodyReference == null)
        {

            t_RigidbodyReference = gameObject.AddComponent<Rigidbody>();
        }

        t_RigidbodyReference.useGravity = false;
        t_RigidbodyReference.isKinematic = true;

        rigidbodyReference = t_RigidbodyReference;

        CapsuleCollider t_CapsulColliderReference = gameObject.GetComponent<CapsuleCollider>();
        if (t_CapsulColliderReference == null)
        {

            t_CapsulColliderReference = gameObject.AddComponent<CapsuleCollider>();
        }

        t_CapsulColliderReference.center = Vector3.up * 0.9f;
        t_CapsulColliderReference.height = 1.8f;
        t_CapsulColliderReference.isTrigger = false;
    }

#endif

    public void PreProcess(int t_NumberOfIdleAnimationClip = 0) {

        DisableRagdoll();

        if (!gameObject.activeInHierarchy)
            gameObject.SetActive(true);

        PlayRandomIdleAnimation(t_NumberOfIdleAnimationClip);

        m_IsAbleToRecieveImpact = true;
    }

    public void PostProcess() {

        if (gameObject.activeInHierarchy)
            gameObject.SetActive(false);

        m_IsAbleToRecieveImpact = false;

    }

    public void ApplyForceOnImpact(Vector3 t_ForceDirection = new Vector3(), float t_Force = 0) {

        if (m_IsAbleToRecieveImpact) {

            if(Random.Range(0f,1f) <= 0.5f)
                GameplayController.Instance.ShowSadExpression(transform);

            Vector3 t_AbsoluteForce = (t_ForceDirection == Vector3.zero ? Random.onUnitSphere : t_ForceDirection) * (t_Force == 0 ? reactionForceOnImpact : t_Force);
            t_AbsoluteForce.y = Mathf.Abs(t_AbsoluteForce.y);

            int t_NumberOfRagdollBone = rigidbodyReferenceOfBones.Length;
            for (int i = 0; i < t_NumberOfRagdollBone; i++)
            {
                rigidbodyReferenceOfBones[i].velocity = Vector3.zero;

                rigidbodyReferenceOfBones[i].AddForce(
                        t_AbsoluteForce,
                        forceModeOfReactionOnImpact
                    );

                rigidbodyReferenceOfBones[i].AddTorque(
                        t_AbsoluteForce * 3.0f,
                        forceModeOfReactionOnImpact
                    );
            }

            OnForceImpact();

            m_IsAbleToRecieveImpact = false;

        }
    }

    public void PlayRandomIdleAnimation(int t_NumberOfAnimationClip = 0) {

        int t_AnimationIndex = Mathf.FloorToInt(Random.Range(0f, 1f) * t_NumberOfAnimationClip);
        crowdAnimatorReference.SetTrigger("IDLE" + t_AnimationIndex);
    }

    public void PlayRandomCheeringAnimation(int t_NumberOfAnimationClip = 0)
    {

        int t_AnimationIndex = Mathf.FloorToInt(Random.Range(0f, 1f) * t_NumberOfAnimationClip);
        crowdAnimatorReference.SetTrigger("CHEER" + t_AnimationIndex);
    }

    public void PlayRandomSadAnimation(int t_NumberOfAnimationClip = 0)
    {
        int t_AnimationIndex = Mathf.FloorToInt(Random.Range(0f, 1f) * t_NumberOfAnimationClip);
        crowdAnimatorReference.SetTrigger("SAD" + t_AnimationIndex);
    }

    public Transform GetCrowdMeshTransformReference() {

        return crowdMeshTransformReference;
    }

    #endregion
}
