﻿using UnityEngine;
using com.faithstudio.Math;
using com.faithstudio.Gameplay;

public class EnemyController : MonoBehaviour
{

    #region Public Variables

    public JoustingKnightController joustingKnightControllerReference;

    [Header("Configuretion  :   EnemyHealth")]
    public AnimationCurve   enemyHealthThroughProgression;
    [Range(-0.125f,0f)]
    public float            lowerBoundForHealth;
    [Range(0f,0.25f)]
    public float            upperBoundForHealth;

    [Header("Configuretion  :   EnemyAttackPower")]
    public AnimationCurve   enemyAttackPowerThroughProgression;
    [Range(-0.125f, 0f)]
    public float            lowerBoundForAttackPower;
    [Range(0f, 0.25f)]
    public float            upperBoundForAttackPower;

    [Header("Configuretion  :   AttackType")]
    public AnimationCurve   curveForProbabilityForCriticalStrike;
    public AnimationCurve   curveForProbabilityForBasicHit;
    public AnimationCurve   curveForProbabilityForMiss;

    #endregion

    #region Private Variables

    private Transform m_TransformReference;

    #endregion

    #region Mono Behaviour

    private void Awake()
    {
        m_TransformReference = transform;
    }

    #endregion

    #region Public Variables

    public void PreProcess(
        float t_Health,
        float t_AttackPower,
        float t_AttackPowerOfOpponent,
        Transform enemyTransformReference)
    {

        joustingKnightControllerReference.PreProcess(
            t_Health,
            t_AttackPower,
            t_AttackPowerOfOpponent,
            m_TransformReference,
            enemyTransformReference,
            null,
            ShowExpressionForLosingHealth
            ,
            delegate{
                
                HapticFeedbackController.Instance.TapPeekVibrate();
                GameplayController.Instance.cameraMovementControllerReference.FocusCameraWithOrigin(
                                                    joustingKnightControllerReference.GetRagdollBodyPartReference()[0],
                                                    joustingKnightControllerReference.GetRagdollBodyPartReference(),
                                                    false,
                                                    Vector3.up * 2.5f,
                                                    new Vector3(-10f, 5, -10f),
                                                    0.1f,
                                                    0.5f,
                                                    1f,
                                                    GameplayController.Instance.cameraAngulerRotationOnJoustingAttacking
                                                );
            }, 
            UIStateController.Instance.UpdateOpponentHealth,
            UIStateController.Instance.UpdateCoinEarnForThisLevel);
    }

    public void PostProcess()
    {


    }

    public void ShowExpressionForLosingHealth(float t_AmountOfHealthReduce){

        HapticFeedbackController.Instance.TapPeekVibrate();
        CharacterExpressionController.Instance.ShowExpression(
                                    1f,
                                    new Vector3(0f,6.25f,5f),
                                    transform,
                                    GameplayController.Instance.expressionForHit,
                                    "-" + t_AmountOfHealthReduce.ToString("F0"),
                                    Color.green
                                );
    }

    public float GetModifiedEnemyHealth(float t_GameProgression, float t_BaseHealth = 0) {

        return t_BaseHealth + ((t_BaseHealth == 0 ? 1 : t_BaseHealth) * Random.Range(lowerBoundForHealth, upperBoundForHealth) * enemyHealthThroughProgression.Evaluate(Mathf.Clamp01(t_GameProgression)));
    }

    public float GetModifiedEnemyAttackPower(float t_GameProgression, float t_BaseAttackPower = 0) {

        return t_BaseAttackPower + ((t_BaseAttackPower == 0 ? 1 : t_BaseAttackPower) * Random.Range(lowerBoundForHealth, upperBoundForHealth) * enemyHealthThroughProgression.Evaluate(Mathf.Clamp01(t_GameProgression)));
    }

    public float GetAttackPerformance(float t_LevelProgression, float t_AttackAccurecy){

        float[] t_ProbabilityOfAttackType = new float[3]{
            curveForProbabilityForCriticalStrike.Evaluate(t_LevelProgression) * Mathf.Clamp(1f - t_AttackAccurecy,0.2f, 0.4f),
            curveForProbabilityForBasicHit.Evaluate(t_LevelProgression) * Mathf.Clamp(1f - t_AttackAccurecy,0.4f, 0.6f),
            curveForProbabilityForMiss.Evaluate(t_LevelProgression) * (1f - t_AttackAccurecy)
        };

        float t_Probability = Random.Range(0f,1f);
        PriorityBound[] t_PriorityBoundForAttack = MathFunction.Instance.GetPriorityBound(t_ProbabilityOfAttackType);
        for(int i = 0 ; i < 3; i++){

            if(t_Probability >= t_PriorityBoundForAttack[i].lowerPriority && t_Probability < t_PriorityBoundForAttack[i].higherPriority){

                switch(i){
                    case 0:
                        return Random.Range(0.75f,0.9f);
                    case 1:
                        return Random.Range(0.4f,0.6f);;
                    case 2:
                        return 0f;
                }
            }
        }

        return Random.Range(0.4f,0.6f);
    }

    #endregion
}
